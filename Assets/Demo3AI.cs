﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class Demo3AI : MonoBehaviour {

    public GameObject[] units;
    bool[] hasSpawned = { false, false, false };
    Animator priest;
    public int alignment;
    float time = 3.0f;
    int unitSpawning = 0;
    bool ambushCompleted = false;
    bool ambushSetUpCompleted = false;
    bool canMakeAmbush = false;
    bool ambushHasBeenInitiated = false;
    bool attackGroupReady = false;
    float ambushTime = 0.0f;
    public string name;
    public Unit_Spawner spawner;
    private List<GameObject> Archers = new List<GameObject>();
    private List<GameObject> Knights = new List<GameObject>();
    private List<GameObject> Priests = new List<GameObject>();
    public List<GameObject> scoutGroup = new List<GameObject>();
    public List<GameObject> attackGroup = new List<GameObject>();
    public List<GameObject> defenseGroup = new List<GameObject>();
    public List<GameObject> siegeGroup = new List<GameObject>();
    public int[] knightEffectiveness = new int[2];
    public int[] archerEffectiveness = new int[2];
    public int[] priestEffectiveness = new int[2];
    unitsBuilt[] buildUnitsOrder;
    public MainBase mb;
    int numKnights = 0;
    int numArchers = 0;
    int numPriests = 0;
    bool old = false;
    Vector3 team1MoveLoc = new Vector3(-70.77477f, 8.490299f, -84.8895f);
    Vector3 team2MoveLoc = new Vector3(87.23208f, 8.490299f, 65.54948f);

    Vector3 archer1Spawnloc = new Vector3(-85.36117f, 1.76716f, -90.28188f);
    Vector3 archer2Spawnloc = new Vector3(85.36117f, 1.76716f, 90.28188f);

    Vector3 knight1Spawnloc = new Vector3(-95.3123f, 1.76716f, -81.62152f);
    Vector3 knight2Spawnloc = new Vector3(95.3123f, 1.76716f, 81.62152f);

    Vector3 priest1Spawnloc = new Vector3(-81.10491f, 1.76716f, -97.35266f);
    Vector3 priest2Spawnloc = new Vector3(81.10491f, 1.76716f, 97.35266f);

    float trainUnitTime = 0.0f;
    public int unitMax = 15;
    float initailWaitTime = 20.0f;
    /// <save data>
    SaveInfo previousGames = new SaveInfo();
    public previousGameInfo currentGame = new previousGameInfo();
    List<BattleData> battlesThisGame = new List<BattleData>();
    List<unitData> unitsBuilt = new List<unitData>();
    List<battleInfo> battles = new List<battleInfo>();
    List<ambushInfo> theAmbushes = new List<ambushInfo>();
    List<ambushInfo> previousAmbushes = new List<ambushInfo>();
    int unitBeingBuilt = 0;
    public bool isTraining = false;
    int currentUnitCount = 0;
    public bool finishedSaving = false;
    /// T/////////// </summary>





    void Start()
    {

        load();
        knightEffectiveness[0] = determineEffectiveness(units[1], units[0]);
        knightEffectiveness[1] = determineEffectiveness(units[1], units[2]);

        archerEffectiveness[0] = determineEffectiveness(units[2], units[0]);
        archerEffectiveness[1] = determineEffectiveness(units[2], units[1]);

        priestEffectiveness[0] = determineEffectiveness(units[0], units[2]);
        priestEffectiveness[1] = determineEffectiveness(units[0], units[1]);
        if (alignment == 1)
        {
            Invoke("spawnKnight", 4.0f);
            Invoke("spawnKnight", 4.0f);
            Invoke("spawnKnight", 4.0f);
            Invoke("spawnArcher", 4.0f);
            Invoke("spawnPriest", 4.0f);
        }
     


    }
    public void updatUnitInfo(int code, unitData theData)
    {
        for (int i = 0; i < unitsBuilt.Count; i++)
        {
            if (unitsBuilt[i].wasCreatedat == theData.wasCreatedat && unitsBuilt[i].thisUnitType == theData.thisUnitType)
            {
                unitsBuilt[i].died = theData.died;
                unitsBuilt[i].numkills = theData.numkills;
            }
        }

    }
    public void AIReset()
    {
        for (int i = 0; i < Archers.Count; i++)
        {
            Destroy(Archers[i]);
        }
        for (int i = 0; i < Knights.Count; i++)
        {
            Destroy(Knights[i]);
        }
        for (int i = 0; i < Priests.Count; i++)
        {
            Destroy(Priests[i]);
        }
        Archers = new List<GameObject>();
        Knights = new List<GameObject>();
        Priests = new List<GameObject>();
        attackGroup = new List<GameObject>();
        siegeGroup = new List<GameObject>();
        scoutGroup = new List<GameObject>();
        defenseGroup = new List<GameObject>();
        currentGame = new previousGameInfo();
        battlesThisGame = new List<BattleData>();
        unitsBuilt = new List<unitData>();
        battles = new List<battleInfo>();
        unitBeingBuilt = 0;
        hasSpawned[0] = false;
        hasSpawned[1] = false;
        hasSpawned[2] = false;
        numKnights = 0;
        numArchers = 0;
        numPriests = 0;
        ambushCompleted = false;
        ambushSetUpCompleted = false;
        canMakeAmbush = false;
        ambushHasBeenInitiated = false;
        attackGroupReady = false;
        ambushTime = 0.0f;
        time = 3.0f;
        finishedSaving = false;
    }
    void spawnPriest()
    {
        if (alignment == 1)
        {
            Priests.Add(spawner.Spawnunit(units[0], priest1Spawnloc));

        }
        else
        {
            Priests.Add(spawner.Spawnunit(units[0], priest2Spawnloc));

        }
        unitData temp = new unitData();
        temp.thisUnitType = unitTypes.Priest;
        temp.wasCreatedat = Time.timeSinceLevelLoad;
        unitsBuilt.Add(temp);

        Priests[Priests.Count - 1].GetComponent<unitCharacter>().myData = temp;
        Priests[Priests.Count - 1].GetComponent<unitCharacter>().keyNum = (unitsBuilt.Count - 1);
        currentUnitCount++;
        hasSpawned[1] = true;
    }
    void spawnKnight()
    {
        hasSpawned[2] = true;
        if (alignment == 1)
        {

            Knights.Add(spawner.Spawnunit(units[1], knight1Spawnloc));

        }
        else
        {
            Knights.Add(spawner.Spawnunit(units[1], knight2Spawnloc));


        }
        unitData temp = new unitData();
        temp.thisUnitType = unitTypes.Knight;
        temp.wasCreatedat = Time.timeSinceLevelLoad;
        unitsBuilt.Add(temp);

        Knights[Knights.Count - 1].GetComponent<unitCharacter>().myData = temp;
        Knights[Knights.Count - 1].GetComponent<unitCharacter>().keyNum = (unitsBuilt.Count - 1);
        currentUnitCount++;
    }
    void spawnArcher()
    {
        if (alignment == 1)
        {
            Archers.Add(spawner.Spawnunit(units[2], archer1Spawnloc));

        }
        else
        {
            Archers.Add(spawner.Spawnunit(units[2], archer2Spawnloc));

        }
        unitData temp = new unitData();
        temp.thisUnitType = unitTypes.Archer;
        temp.wasCreatedat = Time.timeSinceLevelLoad;
        unitsBuilt.Add(temp);

        Archers[Archers.Count - 1].GetComponent<unitCharacter>().myData = temp;
        Archers[Archers.Count - 1].layer = 10;
        Archers[Archers.Count - 1].GetComponent<unitCharacter>().keyNum = (unitsBuilt.Count - 1);
        currentUnitCount++;
        hasSpawned[0] = true;

    }
    


    // Update is called once per frame
    void Update()
    {
        for (int i = 0; i < attackGroup.Count; i++)
        {
            if (attackGroup[i] == null)
            {
                attackGroup.RemoveAt(i);
            }
        }
        for (int i = 0; i < Knights.Count; i++)
        {
            if (Knights[i] == null)
            {
                Knights.RemoveAt(i);
                return;
            }
            
            Knights[i].GetComponent<unitCharacter>().alignment = alignment;
            unitData tempUnit = new unitData();
            tempUnit.thisUnitType = unitTypes.Knight;

            if (!(attackGroup.Contains(Knights[i])))
            {

                attackGroup.Add(Knights[i]);
            }


        }
        for (int i = 0; i < Priests.Count; i++)
        {
            if (Priests[i] == null)
            {
                Priests.RemoveAt(i);
                return;
            }
           
            Priests[i].GetComponent<unitCharacter>().alignment = alignment;
            unitData tempUnit = new unitData();
            tempUnit.thisUnitType = unitTypes.Priest;

            if (!(attackGroup.Contains(Priests[i])))
            {

                attackGroup.Add(Priests[i]);
            }

        }
        for (int i = 0; i < Archers.Count; i++)
        {
            if (Archers[i] == null)
            {
                Archers.RemoveAt(i);
                return;
            }
           
            Archers[i].GetComponent<unitCharacter>().alignment = alignment;
            unitData tempUnit = new unitData();
            tempUnit.thisUnitType = unitTypes.Archer;

            if (!(attackGroup.Contains(Archers[i])))
            {

                attackGroup.Add(Archers[i]);
            }

        }
        if (attackGroup.Count >= 5)
        {
            attackGroupReady = true;
        }
        if (attackGroupReady)
        {
            if (trainUnitTime >= 3)
            {
                if (!ambushCompleted)
                {
                    ambush();
                }
                else if (ambushTime <= 0)
                {
                    ambushCompleted = false;
                }
                else
                {
                    ambushTime -= Time.deltaTime;
                }
                evaluateBattles();
            }
            else
            {
                trainUnitTime += Time.deltaTime;
            }
        }
    }

    public void fillinUnitData()
    {
        for (int i = 0; i < Archers.Count; i++)
        {
            if (Archers[i] != null)
            {
                Archers[i].GetComponent<unitCharacter>().updateData();
                updatUnitInfo(Archers[i].GetComponent<unitCharacter>().keyNum, Archers[i].GetComponent<unitCharacter>().myData);
            }
        }
        for (int i = 0; i < Knights.Count; i++)
        {
            if (Knights[i] != null)
            {
                Knights[i].GetComponent<unitCharacter>().updateData();
                updatUnitInfo(Knights[i].GetComponent<unitCharacter>().keyNum, Knights[i].GetComponent<unitCharacter>().myData);
            }
        }
        for (int i = 0; i < Priests.Count; i++)
        {
            if (Priests[i] != null)
            {
                Priests[i].GetComponent<unitCharacter>().updateData();
                updatUnitInfo(Priests[i].GetComponent<unitCharacter>().keyNum, Priests[i].GetComponent<unitCharacter>().myData);
            }
        }
    }
    public void save()
    {
        fillinUnitData();
        BinaryFormatter bf = new BinaryFormatter();
        File.Delete("D:/Capstone game" + "/" + name + ".dat");
        FileStream file = File.Open("D:/Capstone game" + "/" + name + ".dat", FileMode.OpenOrCreate);
        for (int i = 0; i < battles.Count; i++)
        {
            BattleData temp = new BattleData();
            temp.enemyUnits = battles[i].enemyUnits;
            temp.friendlyUnits = battles[i].friendlyUnits;
            temp.enemyUnitsSurvived = battles[i].enemyUnitsSurvived;
            temp.friendlyUnitsSurvived = battles[i].friendlyUnitsSurvived;
            temp.numEnemyUnits = battles[i].numEnemyUnits;
            temp.numFriendlyUnits = battles[i].numFriendlyUnits;
            temp.won = battles[i].won;
            battlesThisGame.Add(temp);


        }

        currentGame.gameBattles.AddRange(battlesThisGame);
        currentGame.numFriendlyUnitsKilled = unitsBuilt.Count - (Archers.Count + Priests.Count + Knights.Count);
        currentGame.unitsBuilt = unitsBuilt;
        currentGame.ambusheData = theAmbushes;
        previousGames.previousGames.Add(currentGame);
        bf.Serialize(file, previousGames);
        file.Close();
        finishedSaving = true;

    }
    public void load()
    {
        if (File.Exists("D:/Capstone game" + "/" + name + ".dat"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open("D:/Capstone game" + "/" + name + ".dat", FileMode.Open);
            previousGames = (SaveInfo)bf.Deserialize(file);

            file.Close();
            old = true;
            for (int i = 0; i < previousGames.previousGames.Count; i++)
            {
                for (int c = 0; c < previousGames.previousGames[i].ambusheData.Count; c++)
                {
                    if (!previousAmbushes.Contains(previousGames.previousGames[i].ambusheData[c]))
                    {
                        previousAmbushes.Add(previousGames.previousGames[i].ambusheData[c]);
                    }
                }

            }

        }

    }

    private void findBestUnitCombo()
    {
        for (int i = 0; i < previousGames.previousGames.Count; i++)
        {

            for (int b = 0; b < previousGames.previousGames[i].unitsBuilt.Count; b++)
            {
                evaluateUnit(previousGames.previousGames[i].unitsBuilt[b], b);



            }
        }

    }
    private void evaluateUnit(unitData theUnit, int pos)
    {

        int assistCount = 0;
        int killCount = 0;
        int deathCount = 0;
        for (int i = 0; i < previousGames.previousGames.Count; i++)
        {

            if (pos < previousGames.previousGames[i].unitsBuilt.Count)
            {
                if (previousGames.previousGames[i].unitsBuilt[pos] == theUnit)
                {
                    killCount += previousGames.previousGames[i].unitsBuilt[pos].numkills;
                    assistCount += previousGames.previousGames[i].unitsBuilt[pos].numAssists;
                    if (previousGames.previousGames[i].unitsBuilt[pos].died)
                    {
                        deathCount++;
                    }

                }
            }

        }

        float kilDethRatio = findRatio(killCount, deathCount);
        float assistRatio = findRatio(assistCount, deathCount);
        unitsBuilt builtUnit = new unitsBuilt();
        builtUnit.theunit = theUnit;
        builtUnit.killDeatRatio = kilDethRatio;
        builtUnit.assistDeathRatio = assistRatio;
        if (kilDethRatio > .5f && assistRatio > .5f)
        {
            if (buildUnitsOrder[pos] == null)
            {
                buildUnitsOrder[pos] = builtUnit;
            }
            else
            {
                if (buildUnitsOrder[pos].killDeatRatio < builtUnit.killDeatRatio)
                {
                    buildUnitsOrder[pos] = builtUnit;
                }
            }
        }
        else if (kilDethRatio > 1.0 && assistRatio < .5f)
        {


            if (buildUnitsOrder[pos] == null)
            {
                buildUnitsOrder[pos] = builtUnit;
            }
            else
            {
                if (buildUnitsOrder[pos].killDeatRatio < builtUnit.killDeatRatio)
                {
                    buildUnitsOrder[pos] = builtUnit;
                }
            }
        }
        else
        {
            builtUnit.theunit = differentUnit(builtUnit.theunit.thisUnitType);
            builtUnit.killDeatRatio = 0;
            builtUnit.assistDeathRatio = 0;
            buildUnitsOrder[pos] = builtUnit;

        }


    }
    public unitData differentUnit(unitTypes unWantedUnit)
    {
        unitData theReturnUnit = new unitData();
        int chosenOne = Random.Range(0, 2);
        if (unitTypes.Archer == unWantedUnit)
        {
            if (chosenOne == 0)
            {
                theReturnUnit.thisUnitType = unitTypes.Knight;
            }
            else
            {
                theReturnUnit.thisUnitType = unitTypes.Priest;
            }

        }
        else if (unitTypes.Knight == unWantedUnit)
        {

            if (chosenOne == 0)
            {
                theReturnUnit.thisUnitType = unitTypes.Archer;
            }
            else
            {
                theReturnUnit.thisUnitType = unitTypes.Priest;
            }
        }
        else
        {
            if (chosenOne == 0)
            {
                theReturnUnit.thisUnitType = unitTypes.Knight;
            }
            else
            {
                theReturnUnit.thisUnitType = unitTypes.Archer;
            }
        }
        return theReturnUnit;
    }
    public float findRatio(int kills, int deaths)
    {
        float ratio = 0;
        if (deaths == 0)
        {
            ratio = kills;

        }
        else if (kills == 0 && deaths > 0)
        {
            ratio = -1.0f;
        }
        else
        {
            ratio = (float)kills / (float)deaths;
        }
        return ratio;
    }


    private void moveUnits(Vector3 posistion, List<GameObject> theUnits)
    {
        for (int i = 0; i < theUnits.Count; i++)
        {

            if (theUnits[i].GetComponent<unitCharacter>() != null)
            {
                if (!theUnits[i].GetComponent<unitCharacter>().isRetreating)
                {
                    theUnits[i].GetComponent<unitCharacter>().moveTarget = new Vector3(posistion.x, theUnits[i].transform.position.y, posistion.z);
                }
            }
        }
    }
    private void moveUnits2(Vector3 posistion, List<BaseCharacter> theUnits)
    {
        for (int i = 0; i < theUnits.Count; i++)
        {
            if (theUnits[i] != null)
            {
                if (theUnits[i].gameObject.GetComponent<unitCharacter>() != null)
                {

                    theUnits[i].gameObject.GetComponent<unitCharacter>().attackTarget = null;
                    theUnits[i].gameObject.GetComponent<unitCharacter>().moveTarget = new Vector3(posistion.x, theUnits[i].transform.position.y, posistion.z);
                    theUnits[i].gameObject.GetComponent<unitCharacter>().isRetreating = true;
                }
            }
        }
    }
    private void siegeEnemies()
    {
        MainBase[] mb = FindObjectsOfType<MainBase>();
        for (int i = 0; i < siegeGroup.Count; i++)
        {

            if (!siegeGroup[i].GetComponent<unitCharacter>().isHealing && !siegeGroup[i].GetComponent<unitCharacter>().isRetreating)
            {
                if (mb[0].alignment != alignment)
                {

                    siegeGroup[i].GetComponent<unitCharacter>().moveTarget = new Vector3(mb[0].gameObject.transform.position.x, siegeGroup[i].transform.position.y, mb[0].gameObject.transform.position.z);
                }
                else
                {
                    siegeGroup[i].GetComponent<unitCharacter>().moveTarget = new Vector3(mb[1].gameObject.transform.position.x, siegeGroup[i].transform.position.y, mb[1].gameObject.transform.position.z);
                }
            }
        }
    }



    private void scoutMove()
    {
        for (int i = 0; i < scoutGroup.Count; i++)
        {
            if (!scoutGroup[i].GetComponent<unitCharacter>().isRetreating || !scoutGroup[i].GetComponent<unitCharacter>().isHealing)
            {
                if (scoutGroup[i].GetComponent<NavMeshAgent>().remainingDistance <= 2)
                {
                    Vector3 temp;
                    do
                    {
                        temp = new Vector3(Random.Range(-100, 100), scoutGroup[i].transform.position.y, Random.Range(-100, 100));
                    } while ((temp.x <= -52 && temp.z < -52) || (temp.x > 52 && temp.z > 52));
                    scoutGroup[i].GetComponent<unitCharacter>().moveTarget = temp;
                }
            }
        }
    }
    private int determineEffectiveness(GameObject mainUnit, GameObject otherUnit)
    {
        int effectiveness = 0;
        unitCharacter mainUnit2 = mainUnit.GetComponent<unitCharacter>();
        unitCharacter otherUnit2 = otherUnit.GetComponent<unitCharacter>();

        if (mainUnit2.attackRange > otherUnit2.attackRange)
        {
            effectiveness++;
        }
        if (mainUnit2.damage > otherUnit2.damage)
        {
            effectiveness++;
        }
        if (mainUnit2.fireRate > otherUnit2.fireRate)
        {
            effectiveness++;
        }
        if (mainUnit2.totalHealth > otherUnit2.totalHealth)
        {
            effectiveness++;
        }
        return effectiveness;
    }
    private void abandonBattle(battleInfo b)
    {

        for (int i = 0; i < previousGames.previousGames.Count; i++)
        {
            for (int j = 0; j < previousGames.previousGames[i].gameBattles.Count; j++)
            {
                if (previousGames.previousGames[i].gameBattles[j].friendlyUnits.Count == b.friendlyUnits.Count &&
                   previousGames.previousGames[i].gameBattles[j].enemyUnits.Count == b.enemyUnits.Count)
                {
                    if (CompareWithUnitTypes(b.friendlyUnits, previousGames.previousGames[i].gameBattles[j].friendlyUnits) && CompareWithUnitTypes(b.enemyUnits, previousGames.previousGames[i].gameBattles[j].enemyUnits)
                       && !previousGames.previousGames[i].gameBattles[j].won)
                    {
                        if (alignment == 1)
                        {
                            moveUnits2(team1MoveLoc, b.friendlyUnits2);
                        }
                        else
                        {
                            moveUnits2(team2MoveLoc, b.friendlyUnits2);
                        }
                        return;
                    }
                }
            }
        }
    }
    bool CompareWithUnitTypes(List<unitData> one, List<unitData> two)
    {
        List<unitTypes> onesTypes = new List<unitTypes>();
        List<unitTypes> twosTypes = new List<unitTypes>();
        for (int i = 0; i < one.Count; i++)
        {
            onesTypes.Add(one[i].thisUnitType);
        }
        for (int i = 0; i < two.Count; i++)
        {
            twosTypes.Add(two[i].thisUnitType);
        }
        return UnorderedEqual(onesTypes, twosTypes);
    }
    bool UnorderedEqual(ICollection<unitTypes> a, ICollection<unitTypes> b)
    {
        // 1
        // Require that the counts are equal
        if (a.Count != b.Count)
        {
            return false;
        }
        // 2
        // Initialize new Dictionary of the type
        Dictionary<unitTypes, int> d = new Dictionary<unitTypes, int>();
        // 3
        // Add each key's frequency from collection A to the Dictionary
        foreach (unitTypes item in a)
        {
            int c;
            if (d.TryGetValue(item, out c))
            {
                d[item] = c + 1;
            }
            else
            {
                d.Add(item, 1);
            }
        }
        // 4
        // Add each key's frequency from collection B to the Dictionary
        // Return early if we detect a mismatch
        foreach (unitTypes item in b)
        {
            int c;
            if (d.TryGetValue(item, out c))
            {
                if (c == 0)
                {
                    return false;
                }
                else
                {
                    d[item] = c - 1;
                }
            }
            else
            {
                // Not in dictionary
                return false;
            }
        }
        // 5
        // Verify that all frequencies are zero
        foreach (int v in d.Values)
        {
            if (v != 0)
            {
                return false;
            }
        }
        // 6
        // We know the collections are equal
        return true;
    }


    private void evaluateBattles()
    {
        for (int b = 0; b < battles.Count; b++)
        {
            if (!battles[b].evaluated)
            {
                abandonBattle(battles[b]);
                if (battles[b].friendlyUnits2.Count <= 0)
                {
                    battles[b].won = false;
                    battles[b].evaluated = true;
                    battles[b].friendlyUnitsSurvived = 0;
                    battles[b].enemyUnitsSurvived = battles[b].enemyUnits.Count - battles[b].enemyUnits2.Count;
                }
                else if (battles[b].enemyUnits2.Count <= 0)
                {
                    battles[b].won = true;
                    battles[b].evaluated = true;
                    battles[b].enemyUnitsSurvived = 0;
                    battles[b].friendlyUnitsSurvived = battles[b].friendlyUnits.Count - battles[b].friendlyUnits2.Count;
                }
                else
                {
                    for (int i = 0; i < battles[b].friendlyUnits2.Count; i++)
                    {

                        if (battles[b].friendlyUnits2[i].isRetreating)
                        {
                            battles[b].won = false;
                            battles[b].evaluated = true;
                            battles[b].friendlyUnitsSurvived = battles[b].friendlyUnits.Count - battles[b].friendlyUnits2.Count;
                            battles[b].enemyUnitsSurvived = battles[b].enemyUnits.Count - battles[b].enemyUnits2.Count;
                            return;
                        }
                        if (battles[b].friendlyUnits2[i] == null)
                        {
                            battles[b].friendlyUnits2.RemoveAt(i);
                        }
                    }
                    for (int i = 0; i < battles[b].enemyUnits2.Count; i++)
                    {

                        if (battles[b].enemyUnits2[i].isRetreating)
                        {
                            battles[b].won = true;
                            battles[b].evaluated = true;
                            battles[b].friendlyUnitsSurvived = battles[b].friendlyUnits.Count - battles[b].friendlyUnits2.Count;
                            battles[b].enemyUnitsSurvived = battles[b].enemyUnits.Count - battles[b].enemyUnits2.Count;
                            return;
                        }
                        if (battles[b].enemyUnits2[i] == null)
                        {
                            battles[b].enemyUnits2.RemoveAt(i);
                        }
                    }
                }
            }
        }
    }
    private bool isValidBattle(BaseCharacter bc)
    {
        bool valid = true;
        for (int b = 0; b < battles.Count; b++)
        {
            if (!battles[b].evaluated && battles[b].friendlyUnits2.Contains(bc))
            {
                valid = false;
            }
        }
        return valid;
    }
    private battleInfo battle(List<GameObject> groups,ref battleInfo bi)
    {
        BaseCharacter[] allUnits = FindObjectsOfType<BaseCharacter>();



        for (int i = 0; i < groups.Count; i++)
        {
            if (groups[i].GetComponent<unitCharacter>().engagedIncombat)
            {

                if (isValidBattle(groups[i].GetComponent<BaseCharacter>()))
                {
                    battleInfo b = new battleInfo();
                    unitData temp = groups[i].GetComponent<BaseCharacter>().myData;

                    for (int j = 0; j < allUnits.Length; j++)
                    {
                        temp = allUnits[j].myData;
                        if (allUnits[j].alignment == alignment && (allUnits[j].transform.position - groups[i].transform.position).magnitude <= 20 && allUnits[j].damage != 0)
                        {
                            b.friendlyUnits2.Add(allUnits[j]);

                            b.friendlyUnits.Add(temp);

                        }
                        else if (allUnits[j].alignment != alignment && (allUnits[j].transform.position - groups[i].transform.position).magnitude <= 20 && allUnits[j].damage != 0)
                        {

                            b.enemyUnits2.Add(allUnits[j]);
                            b.enemyUnits.Add(temp);
                        }

                    }
                    b.numEnemyUnits = b.enemyUnits.Count;
                    b.numFriendlyUnits = b.friendlyUnits.Count;

                    if (bi != null)
                    {
                        
                        battles.Add(bi);
                      bi= battles[battles.Count - 1];

                    }
                    else
                    {
                        battles.Add(b);
                    }
                        return battles[battles.Count-1];
                    
                }
            }

        }
        return null;
    }
    private void withDraw(List<GameObject> groups)
    {
        BaseCharacter[] allUnits = FindObjectsOfType<BaseCharacter>();


        for (int i = 0; i < groups.Count; i++)
        {
            int canWin = 0;
            int cantWin = 0;
            List<BaseCharacter> friendlies = new List<BaseCharacter>();
            List<BaseCharacter> enemies = new List<BaseCharacter>();
            if (groups[i].GetComponent<unitCharacter>().engagedIncombat)
            {
                friendlies.Add(groups[i].GetComponent<BaseCharacter>());
                for (int j = 0; j < allUnits.Length; j++)
                {
                    if (allUnits[j].alignment == alignment && (allUnits[j].transform.position - groups[i].transform.position).magnitude <= 20 && allUnits[j].damage != 0)
                    {
                        friendlies.Add(allUnits[j]);
                        canWin++;
                    }
                    else if (allUnits[j].alignment != alignment && (allUnits[j].transform.position - groups[i].transform.position).magnitude <= 20 && allUnits[j].damage != 0)
                    {
                        enemies.Add(allUnits[j]);
                        cantWin++;
                    }
                }
                if (evaluateEffectiveNess(friendlies) >= evaluateEffectiveNess(enemies))
                {
                    canWin++;
                }
                else
                {
                    cantWin++;
                }
                if (canWin < cantWin)
                {
                    for (int j = 0; j < friendlies.Count; j++)
                    {
                        if (friendlies[j].gameObject.GetComponent<unitCharacter>() != null)
                        {
                            if (alignment == 1)
                            {
                                friendlies[j].gameObject.GetComponent<unitCharacter>().moveTarget = new Vector3(team1MoveLoc.x, friendlies[j].transform.position.y, team1MoveLoc.z);

                            }
                            else
                            {
                                friendlies[j].gameObject.GetComponent<unitCharacter>().moveTarget = new Vector3(team2MoveLoc.x, friendlies[j].transform.position.y, team2MoveLoc.z);
                            }
                            friendlies[j].gameObject.GetComponent<unitCharacter>().attackTarget = null;
                            friendlies[j].gameObject.GetComponent<unitCharacter>().isRetreating = true;
                        }
                    }
                }


            }
        }
    }
    private int evaluateEffectiveNess(List<BaseCharacter> bc)
    {
        int evalResult = 0;
        for (int i = 0; i < bc.Count; i++)
        {
            if (bc[i].health < bc[i].totalHealth * .5)
            {
                evalResult -= 1;
            }
            else if (bc[i].health > bc[i].totalHealth * .75)
            {
                evalResult += 1;
            }
        }
        return evalResult;
    }
    Vector3 ambushPosition = new Vector3();
    bool ambushRetreat = false;
    bool battleRecorded = false;
    bool leadToTrap = false;
    ambushInfo tempAmbush = new ambushInfo();
    float waitBait = 2;
    private void ambush()
    {
        
        battleInfo ambushBattle = null;
        if (!ambushSetUpCompleted)
        {
            leadToTrap = false;
            setUpAmbush();
            ambushSetUpCompleted = true;
        }
        tempAmbush.ambushPositionX = ambushPosition.x;
        tempAmbush.ambushPositionY = ambushPosition.y;
        tempAmbush.ambushPositionZ = ambushPosition.z;
        if (!leadToTrap)
        {
            if (attackGroup[0].GetComponent<unitCharacter>().engagedIncombat && !ambushRetreat)
            {
                attackGroup[0].GetComponent<unitCharacter>().moveTarget = new Vector3(ambushPosition.x, attackGroup[0].transform.position.y, ambushPosition.z);

                attackGroup[0].GetComponent<unitCharacter>().attackTarget = null;
                attackGroup[0].GetComponent<unitCharacter>().isRetreating = true;
                tempAmbush.baitInitaitePosX = attackGroup[0].transform.position.x;
                tempAmbush.baitInitaitePosY = attackGroup[0].transform.position.y;
                tempAmbush.baitInitaitePosZ = attackGroup[0].transform.position.z;

                ambushHasBeenInitiated = true;
                ambushRetreat = true;
            }
            else
            {
                if (waitBait <= 0)
                {
                    baitMovement();
                }
                else
                {
                    waitBait -= Time.deltaTime;
                }
            }
            if (ambushRetreat)
            {
                attackGroup[0].GetComponent<unitCharacter>().attackTarget = null;
                attackGroup[0].GetComponent<unitCharacter>().isRetreating = true;
                attackGroup[0].GetComponent<unitCharacter>().moveTarget = new Vector3(ambushPosition.x, attackGroup[0].transform.position.y, ambushPosition.z);
              


            }
        }
        if ((attackGroup[0].transform.position - new Vector3(ambushPosition.x, attackGroup[0].transform.position.y, ambushPosition.z)).magnitude < 5.0f)
        {
            if (ambushBattle == null && attackGroup[1].GetComponent<unitCharacter>().engagedIncombat && !battleRecorded)
            {
                ambushRetreat = false;
                leadToTrap = true;
                attackGroup[0].GetComponent<unitCharacter>().isRetreating = false;
                battleRecorded = true;
                ambushBattle = new battleInfo();
                 battle(attackGroup,ref ambushBattle);
            }
        }
        if (ambushBattle != null)
        {
         
                tempAmbush.wasSuccesFull = true;
                ambushWasCompleted();
                if (!theAmbushes.Contains(tempAmbush))
                {
                    theAmbushes.Add(tempAmbush);
                        ambushBattle=null;
                        tempAmbush = new ambushInfo();
                }
            
        }
    }
    private void ambushWasCompleted()
    {
        if (ambushPosition != Vector3.zero && ambushHasBeenInitiated)
        {
            ambushCompleted = true;
            ambushSetUpCompleted = false;
            ambushTime = 45.0f;
        }
    }
    int ambushCount = 0;
    private void baitMovement()
    {

        if (attackGroup[0].GetComponent<NavMeshAgent>().remainingDistance <= 2)
        {
            Vector3 temp;
            do
            {
                temp = new Vector3(Random.Range(-100, 100), attackGroup[0].transform.position.y, Random.Range(-100, 100));
            } while ((temp.x <= -15.854 && temp.z < -52) || (temp.x > 52 && temp.z > 52));
            attackGroup[0].GetComponent<unitCharacter>().moveTarget = temp;
        }
    }

    private void setUpAmbush()
    {

        if (!old || ambushCount >= previousAmbushes.Count)
        {
            do
            {
               ambushPosition = new Vector3(Random.Range(-100, 100), attackGroup[0].transform.position.y, Random.Range(-100, 100));
            } while ((ambushPosition.x >= -52 && ambushPosition.z > -52) || (ambushPosition.x < -3 && ambushPosition.z < 20));

            for (int i = 0; i < attackGroup.Count; i++)
            {

                if (i == 0)
                {
                    attackGroup[i].GetComponent<unitCharacter>().isBait = true;
                }
                attackGroup[i].GetComponent<unitCharacter>().moveTarget = new Vector3(ambushPosition.x, attackGroup[i].transform.position.y, ambushPosition.z);

                attackGroup[i].GetComponent<unitCharacter>().attackTarget = null;
                attackGroup[i].GetComponent<unitCharacter>().isRetreating = true;
            }
            ambushSetUpCompleted = true;
        }
        else
        {
            ambushPosition = new Vector3(previousAmbushes[ambushCount].ambushPositionX,previousAmbushes[ambushCount].ambushPositionY,previousAmbushes[ambushCount].ambushPositionZ);
            for (int i = 0; i < attackGroup.Count; i++)
            {

                if (i == 0)
                {
                    attackGroup[i].GetComponent<unitCharacter>().isBait = true;
                }
                attackGroup[i].GetComponent<unitCharacter>().moveTarget = new Vector3(ambushPosition.x, attackGroup[i].transform.position.y, ambushPosition.z);

                attackGroup[i].GetComponent<unitCharacter>().attackTarget = null;
                attackGroup[i].GetComponent<unitCharacter>().isRetreating = true;
            }
           
            attackGroup[0].GetComponent<unitCharacter>().moveTarget = new Vector3(previousAmbushes[ambushCount].baitInitaitePosX,previousAmbushes[ambushCount].baitInitaitePosY,previousAmbushes[ambushCount].baitInitaitePosZ);
            ambushCount++;

        }
    }
	
}
