Shader "Custom/FogOFWar" {
Properties {
	_Color ("Main Color", Color) = (1,1,1,1)
	_MainTex ("Base (RGB) Trans (A)", 2D) = "white" {}
	_FogRadius ("FogRadius", Float) = 1.0
	_FogMaxRadius("FogMaxRadius", Float) = 0.5
	_FogBaseRadius ("FogBaseRadius", Float) = 1.0
	_FogMaxBaseRadius("FogMaxBaseRadius", Float) = 0.5
	_Player1_pos ("_Player1_pos",Vector)=(0,0,0,1)
	_Player2_pos ("_Player2_pos",Vector)=(-200,0,0,1)
	_Player3_pos ("_Player3_pos",Vector)=(-200,0,0,1)
	_Player4_pos ("_Player4_pos",Vector)=(-200,0,0,1)
	_Player5_pos ("_Player5_pos",Vector)=(-200,0,0,1)
	_Player6_pos ("_Player6_pos",Vector)=(-200,0,0,1)
	_Player7_pos ("_Player7_pos",Vector)=(-200,0,0,1)
	_Player8_pos ("_Player8_pos",Vector)=(-200,0,0,1)
	_Player9_pos ("_Player9_pos",Vector)=(-200,0,0,1)
	_Player10_pos ("_Player10_pos",Vector)=(-200,0,0,1)
	_Player11_pos ("_Player11_pos",Vector)=(-200,0,0,1)
	_Player12_pos ("_Player12_pos",Vector)=(-200,0,0,1)
	_Player13_pos ("_Player13_pos",Vector)=(-200,0,0,1)
	_Player14_pos ("_Player14_pos",Vector)=(-200,0,0,1)
	_Player15_pos ("_Player15_pos",Vector)=(-200,0,0,1)
	_Player16_pos ("_Player16_pos",Vector)=(-200,0,0,1)
	_Player17_pos ("_Player17_pos",Vector)=(-200,0,0,1)
	_Player18_pos ("_Player18_pos",Vector)=(-200,0,0,1)
	_Player19_pos ("_Player19_pos",Vector)=(-200,0,0,1)
	_Player20_pos ("_Player20_pos",Vector)=(-200,0,0,1)
	_Player21_pos ("_Player21_pos",Vector)=(-200,0,0,1)
	_PlayerBase1_pos ("_PlayerBase1_pos",Vector)=(-110.5249,-2.546686,-1.35007,1)
	_PlayerBase2_pos ("_PlayerBase2_pos",Vector)=(-110.5249,-2.546686,-1.35007,1)
	_PlayerBase3_pos ("_PlayerBase3_pos",Vector)=(-110.5249,-2.546686,-1.35007,1)
	_PlayerBase4_pos ("_PlayerBase4_pos",Vector)=(-110.5249,-2.546686,-1.35007,1)
	_PlayerBase5_pos ("_PlayerBase5_pos",Vector)=(-110.5249,-2.546686,-1.35007,1)
	_PlayerBase6_pos ("_PlayerBase6_pos",Vector)=(-110.5249,-2.546686,-1.35007,1)
	_PlayerBase7_pos ("_PlayerBase7_pos",Vector)=(-110.5249,-2.546686,-1.35007,1)
	_PlayerBase8_pos ("_PlayerBase8_pos",Vector)=(-110.5249,-2.546686,-1.35007,1)
	_PlayerBase9_pos ("_PlayerBase9_pos",Vector)=(-110.5249,-2.546686,-1.35007,1)
	_PlayerBase10_pos ("_PlayerBase10_pos",Vector)=(-110.5249,-2.546686,-1.35007,1)
	_PlayerBase11_pos ("_PlayerBase11_pos",Vector)=(-110.5249,-2.546686,-1.35007,1)
}

SubShader {
	Tags {"Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent"}
	LOD 200
	Blend SrcAlpha OneMinusSrcAlpha
	Cull Off

CGPROGRAM
#pragma surface surf Lambert vertex:vert

sampler2D _MainTex;
fixed4 _Color;
float _FogRadius;
float	_FogMaxRadius;
float _FogBaseRadius;
float	_FogMaxBaseRadius;
float4	_Player1_pos; 
float4	_Player2_pos; 
float4	_Player3_pos; 
float4	_Player4_pos; 
float4	_Player5_pos; 
float4	_Player6_pos; 
float4	_Player7_pos; 
float4	_Player8_pos; 
float4	_Player9_pos; 
float4	_Player10_pos; 
float4	_Player11_pos; 
float4	_Player12_pos; 
float4	_Player13_pos;
float4	_Player14_pos;
float4	_Player15_pos;
float4	_Player16_pos;
float4	_Player17_pos;
float4	_Player18_pos;
float4	_Player19_pos;
float4	_Player20_pos;
float4	_Player21_pos;
float4  _PlayerBase1_pos;
float4  _PlayerBase2_pos;
float4  _PlayerBase3_pos;
float4  _PlayerBase4_pos;
float4  _PlayerBase5_pos;
float4  _PlayerBase6_pos;
float4  _PlayerBase7_pos;
float4  _PlayerBase8_pos;
float4  _PlayerBase9_pos;
float4  _PlayerBase10_pos;
float4  _PlayerBase11_pos;

struct Input {
	float2 uv_MainTex;
	float2 location;
};
float powerForPos(float4 pos, float2 nearVertex);
float powerForBasePos(float4 pos, float2 nearVertex);
void vert(inout appdata_full vertexData, out Input outData){
	float4 pos=mul(UNITY_MATRIX_MVP,vertexData.vertex);
	float4 posWorld=mul(_Object2World,vertexData.vertex);
	outData.uv_MainTex=vertexData.texcoord;
	outData.location=posWorld.xz;
}
void surf (Input IN, inout SurfaceOutput o) {
	fixed4 baseColor = tex2D(_MainTex, IN.uv_MainTex) * _Color;
	
	float alpha=(1.0-(baseColor.a+powerForPos(_Player1_pos,IN.location)+powerForPos(_Player2_pos,IN.location)+powerForPos(_Player3_pos,IN.location)
	+powerForPos(_Player4_pos,IN.location)+powerForPos(_Player5_pos,IN.location)
	+powerForPos(_Player5_pos,IN.location)+powerForPos(_Player6_pos,IN.location)+powerForPos(_Player7_pos,IN.location)
	+powerForPos(_Player8_pos,IN.location)+powerForPos(_Player9_pos,IN.location)+powerForPos(_Player10_pos,IN.location)
	+powerForPos(_Player11_pos,IN.location)+powerForPos(_Player12_pos,IN.location)+powerForPos(_Player13_pos,IN.location)
	+powerForPos(_Player14_pos,IN.location)+powerForPos(_Player15_pos,IN.location)+powerForPos(_Player16_pos,IN.location)
	+powerForPos(_Player17_pos,IN.location)+powerForPos(_Player18_pos,IN.location)+powerForPos(_Player19_pos,IN.location)
	+powerForPos(_Player20_pos,IN.location)+powerForPos(_Player21_pos,IN.location)+powerForBasePos(_PlayerBase1_pos,IN.location)
	+powerForBasePos(_PlayerBase2_pos,IN.location)+powerForBasePos(_PlayerBase3_pos,IN.location)+powerForBasePos(_PlayerBase4_pos,IN.location)
	+powerForBasePos(_PlayerBase5_pos,IN.location)+powerForBasePos(_PlayerBase6_pos,IN.location)+powerForBasePos(_PlayerBase7_pos,IN.location)
	+powerForBasePos(_PlayerBase8_pos,IN.location)+powerForBasePos(_PlayerBase9_pos,IN.location)+powerForBasePos(_PlayerBase10_pos,IN.location)
	+powerForBasePos(_PlayerBase11_pos,IN.location)));
	
	o.Albedo = baseColor.rgb;
	o.Alpha = alpha;
}
//return 0 if(pos-nearVertex)>_FogRadius
float powerForPos(float4 pos, float2 nearVertex){
 float atten=clamp(_FogRadius-length(pos.xz-nearVertex.xy),0.0,_FogRadius);
 
 return (1.0/_FogMaxRadius)*atten/_FogRadius;
}
float powerForBasePos(float4 pos, float2 nearVertex){
 float atten=clamp(_FogBaseRadius-length(pos.xz-nearVertex.xy),0.0,_FogBaseRadius);
 
 return (1.0/_FogMaxBaseRadius)*atten/_FogBaseRadius;
}
ENDCG
}

Fallback "Transparent/VertexLit"
}
