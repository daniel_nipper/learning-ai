﻿using UnityEngine;
using System.Collections;

public class FOG_Player : MonoBehaviour {
	public Transform fogPlane;
	public int playerNum;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 screenPos = Camera.main.WorldToScreenPoint (transform.position);
		Ray rayToPlayerPos = Camera.main.ScreenPointToRay (screenPos);
		if (fogPlane == null) {
			fogPlane=GameObject.Find("FogOfWarPlane").transform;		
		}
		RaycastHit hit;
		if (Physics.Raycast (rayToPlayerPos, out hit, 1000)) {
			fogPlane.GetComponent<Renderer>().material.SetVector("_Player"+playerNum+"_pos",new Vector4(transform.position.x,transform.position.y,transform.position.z,1.0f));	
		}
	}
}
