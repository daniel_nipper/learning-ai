﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MainBase : BaseCharacter {

	public bool isBase=true;
	float recoverTime=10.0f;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (health > totalHealth) {
			health=totalHealth;
		}
		if (health < totalHealth) {
				if(recoverTime<=0)
			{
				recoverTime=10.0f;
				health+=20;
			}
			else
			{
				recoverTime-=Time.deltaTime;
			}
		}
	}
	public void MainBaseReset()
	{
		health = totalHealth;
	}
    public List<Vector3> enemiesAttackingPos()
    {
        List<Vector3> positions = new List<Vector3>();
        unitCharacter[] uc =GameObject.FindObjectsOfType<unitCharacter>() ;

        for (int i = 0; i < uc.Length; i++)
        {
            if (uc[i].alignment != this.alignment && Vector3.Distance(transform.position,uc[i].transform.position)<25)
            {
                if (distance(positions, uc[i].transform.position))
                {
                    positions.Add(uc[i].transform.position);
                }
 
            }
        }
        return positions;

    }
    public bool distance(List<Vector3> positions, Vector3 pos)
    {
        bool isNotCovered = true;
        foreach (Vector3 item in positions)
        {
            if (Vector3.Distance(item, pos) <= 20)
            {
                isNotCovered = false;
            }
        }
        return isNotCovered;
    }
}
