﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
public class AI_units : MonoBehaviour {

	// Use this for initialization
	public GameObject[] units;
	bool[] hasSpawned={false,false,false};
	Animator priest;
	public int alignment;
	float time=3.0f;
	int unitSpawning=0;
	bool ambushCompleted=false;
	bool ambushSetUpCompleted=false;
	bool canMakeAmbush=false;
	bool ambushHasBeenInitiated=false;
	bool attackGroupReady=false;
	float ambushTime=0.0f;
	public string name;
	public Unit_Spawner spawner;
	private List<GameObject> Archers=new List<GameObject>();
	private List<GameObject> Knights=new List<GameObject>();
	private List<GameObject> Priests=new List<GameObject>();
	public List<GameObject> scoutGroup = new List<GameObject> ();
	public List<GameObject> attackGroup = new List<GameObject> ();
	public List<GameObject> defenseGroup = new List<GameObject> ();
	public List<GameObject> siegeGroup = new List<GameObject> ();
	public int[] knightEffectiveness = new int[2];
	public int[] archerEffectiveness = new int[2];
	public int[] priestEffectiveness = new int[2];
	unitsBuilt[] buildUnitsOrder;
	public MainBase mb;
	int numKnights=0;
	int numArchers=0;
	int numPriests=0;
	bool old=false;
	Vector3 team1MoveLoc = new Vector3 (-70.77477f,8.490299f,-84.8895f);
	Vector3 team2MoveLoc = new Vector3 (87.23208f,8.490299f,65.54948f);

	Vector3 archer1Spawnloc = new Vector3 (-85.36117f, 1.76716f, -90.28188f);
	Vector3 archer2Spawnloc = new Vector3 (85.36117f, 1.76716f, 90.28188f);

	Vector3 knight1Spawnloc = new Vector3 (-95.3123f, 1.76716f, -81.62152f);
	Vector3 knight2Spawnloc = new Vector3 (95.3123f, 1.76716f, 81.62152f);

	Vector3 priest1Spawnloc = new Vector3 (-81.10491f, 1.76716f, -97.35266f);
	Vector3 priest2Spawnloc = new Vector3 (81.10491f, 1.76716f, 97.35266f);

	float trainUnitTime=0.0f;
	public int unitMax=15;
	float initailWaitTime=20.0f;
/// <save data>
	SaveInfo previousGames=new SaveInfo();
	public previousGameInfo currentGame=new previousGameInfo();
	List<BattleData> battlesThisGame= new List<BattleData>();
	List<unitData> unitsBuilt = new List<unitData> ();
	List<battleInfo> battles=new List<battleInfo>();
    List<ambushInfo> theAmbushes = new List<ambushInfo>();
    List<ambushInfo> previousAmbushes = new List<ambushInfo>();
    List<Vector3> patrolLocations = new List<Vector3>();
    List<defensePatrolInfo> patrolInfo = new List<defensePatrolInfo>();
	int unitBeingBuilt=0;
	public bool isTraining=false;
	int currentUnitCount=0;
	public bool finishedSaving=false;
/// T/////////// </summary>





	void Start () {

						load ();
						knightEffectiveness [0] = determineEffectiveness (units [1], units [0]);
						knightEffectiveness [1] = determineEffectiveness (units [1], units [2]);
		
						archerEffectiveness [0] = determineEffectiveness (units [2], units [0]);
						archerEffectiveness [1] = determineEffectiveness (units [2], units [1]);
		
						priestEffectiveness [0] = determineEffectiveness (units [0], units [2]);
						priestEffectiveness [1] = determineEffectiveness (units [0], units [1]);
						if (old) {
								for (int i=0; i<buildUnitsOrder.Length &&i<3; i++) {
										
										if (buildUnitsOrder [i].theunit.thisUnitType == unitTypes.Archer) {
												Invoke ("spawnArcher", 4.0f);	
										} else if (buildUnitsOrder [i].theunit.thisUnitType == unitTypes.Knight) {
												Invoke ("spawnKnight", 4.0f);
										} else {
												Invoke ("spawnPriest", 4.0f);	
										}
										unitBeingBuilt++;
								}
		
						} else {
				
								Invoke ("spawnPriest", 4.0f);

								Invoke ("spawnKnight", 8.0f);

								Indestructable c = FindObjectOfType<Indestructable> ();
								if (alignment == 2) {
									if(GameObject.Find ("AI_heroManager")!=null)
									{
										if (GameObject.Find ("AI_heroManager").GetComponent<AI_Hero> ().hero.GetComponent<unitHero> ().isArcher) {
												currentGame.alliedHero = heroType.Archer;
										} else {
												currentGame.alliedHero = heroType.Berzerker;
										}
									}
								} else if (c != null) {
										if (c.chosen == 0) {
												currentGame.alliedHero = heroType.Archer;
												Invoke ("spawnKnight", 8.0f);
		
										} else {
												Invoke ("spawnArcher", 12.0f);
												currentGame.alliedHero = heroType.Berzerker;
										}
						
								} else {
										currentGame.alliedHero = heroType.Archer;
								}

						}
	
				
	}
	public void updatUnitInfo(int code, unitData theData)
	{
		for (int i = 0; i < unitsBuilt.Count; i++) {
			if(unitsBuilt[i].wasCreatedat==theData.wasCreatedat && unitsBuilt[i].thisUnitType==theData.thisUnitType)
			{
				unitsBuilt [i].died = theData.died;
				unitsBuilt[i].numkills=theData.numkills;
			}
				}

		}
	public void AIReset()
	{
		for (int i = 0; i < Archers.Count; i++) {
			Destroy(Archers[i]);
				}
		for (int i = 0; i < Knights.Count; i++) {
			Destroy(Knights[i]);
		}
		for (int i = 0; i < Priests.Count; i++) {
			Destroy(Priests[i]);
		}
		Archers = new List<GameObject> ();
		Knights = new List<GameObject> ();
		Priests = new List<GameObject> ();
		attackGroup = new List<GameObject> ();
		siegeGroup = new List<GameObject> ();
		scoutGroup = new List<GameObject> ();
		defenseGroup = new List<GameObject> ();
		currentGame=new previousGameInfo();
		battlesThisGame= new List<BattleData>();
		unitsBuilt = new List<unitData> ();
		battles=new List<battleInfo>();
		unitBeingBuilt=0;
		hasSpawned [0] = false;
		hasSpawned [1] = false;
		hasSpawned [2] =false;
		numKnights=0;
     	numArchers=0;
    	numPriests=0;
		ambushCompleted=false;
        ambushSetUpCompleted=false;
        canMakeAmbush=false;
        ambushHasBeenInitiated=false;
		attackGroupReady=false;
		ambushTime=0.0f;
		time = 3.0f;
		finishedSaving = false;
	}
	void spawnArcher()
	{
		if (alignment == 1) {
			Archers.Add (spawner.Spawnunit (units [2],archer1Spawnloc));

				} else {
			Archers.Add (spawner.Spawnunit (units [2], archer2Spawnloc));

		}
		unitData temp=new unitData();
		temp.thisUnitType = unitTypes.Archer;
		temp.wasCreatedat = Time.timeSinceLevelLoad;
		unitsBuilt.Add (temp);

		Archers [Archers.Count - 1].GetComponent<unitCharacter> ().myData = temp;
		Archers [Archers.Count - 1].layer = 10;
		Archers [Archers.Count - 1].GetComponent<unitCharacter> ().keyNum = (unitsBuilt.Count-1);
		currentUnitCount++;
		hasSpawned [0] = true;

	}
	void spawnPriest()
	{
		if (alignment == 1) {
			Priests.Add (spawner.Spawnunit (units [0], priest1Spawnloc));

				} else {
			Priests.Add (spawner.Spawnunit (units [0],priest2Spawnloc ));

		}
		unitData temp=new unitData();
		temp.thisUnitType = unitTypes.Priest;
		temp.wasCreatedat = Time.timeSinceLevelLoad;
		unitsBuilt.Add (temp);
	
		Priests [Priests.Count - 1].GetComponent<unitCharacter> ().myData = temp;
		Priests [Priests.Count - 1].GetComponent<unitCharacter> ().keyNum = (unitsBuilt.Count-1);
		currentUnitCount++;
		hasSpawned [1] = true;
		}
	void spawnKnight()
	{
		hasSpawned [2] = true;
		if (alignment == 1) {
				
			Knights.Add (spawner.Spawnunit (units [1],knight1Spawnloc ));

				} else {
			Knights.Add (spawner.Spawnunit (units [1],knight2Spawnloc ));


		}
		unitData temp=new unitData();
		temp.thisUnitType = unitTypes.Knight;
		temp.wasCreatedat = Time.timeSinceLevelLoad;
		unitsBuilt.Add (temp);

		Knights [Knights.Count - 1].GetComponent<unitCharacter> ().myData = temp;
		Knights [Knights.Count - 1].GetComponent<unitCharacter> ().keyNum = (unitsBuilt.Count-1);
		currentUnitCount++;
		}
	
	// Update is called once per frame
	void Update () {

		//different groups
		for (int i = 0; i < defenseGroup.Count; i++) {
					if(defenseGroup[i]==null)
					{
						defenseGroup.RemoveAt(i);
					}

				}
		for (int i = 0; i < attackGroup.Count; i++) {
			if(attackGroup[i]==null)
			{
				attackGroup.RemoveAt(i);
			}
		}
		for (int i = 0; i < siegeGroup.Count; i++) {
			if(siegeGroup[i]==null)
			{
				siegeGroup.RemoveAt(i);
			}

		}
		for (int i = 0; i < scoutGroup.Count; i++) {
			if(scoutGroup[i]==null)
			{
				scoutGroup.RemoveAt(i);
			}
		}
		//unit clear
		for (int i=0; i<Knights.Count; i++) {
			if(Knights[i]==null)
			{
				Knights.RemoveAt(i);
				return;
			}
			else
			{
				if(alignment==1)
				{
					if(Knights[i].transform.position.x==knight1Spawnloc.x && Knights[i].transform.position.z==knight1Spawnloc.z)
					{
						Knights[i].GetComponent<unitCharacter>().moveTarget=new Vector3(team1MoveLoc.x,Knights[i].transform.position.y,team1MoveLoc.z);
					}
				}
				else
				{
					if(Knights[i].transform.position.x==knight2Spawnloc.x && Knights[i].transform.position.z==knight2Spawnloc.z)
					{
						Knights[i].GetComponent<unitCharacter>().moveTarget=new Vector3(team2MoveLoc.x,Knights[i].transform.position.y,team2MoveLoc.z);
					}

					}
			}
			Knights[i].GetComponent<unitCharacter>().alignment=alignment;
			unitData tempUnit=new unitData();
			tempUnit.thisUnitType=unitTypes.Knight;
			if(scoutGroup.Count<1 &&!(defenseGroup.Contains(Knights[i]) ||scoutGroup.Contains(Knights[i]) || attackGroup.Contains(Knights[i])|| siegeGroup.Contains(Knights[i])))
			{


				GameObject temp=Knights[i];
				scoutGroup.Add(temp);
			}
			else if(defenseGroup.Count<4 &&!(defenseGroup.Contains(Knights[i]) ||scoutGroup.Contains(Knights[i]) || attackGroup.Contains(Knights[i])|| siegeGroup.Contains(Knights[i])))
			{

				
				defenseGroup.Add(Knights[i]);
			}
			else if(attackGroup.Count<5 &&!(attackGroup.Contains(Knights[i])|| scoutGroup.Contains(Knights[i]) || defenseGroup.Contains(Knights[i])|| siegeGroup.Contains(Knights[i])))
			{
				
				attackGroup.Add(Knights[i]);
			}
			else if(!(attackGroup.Contains(Knights[i])|| scoutGroup.Contains(Knights[i]) || defenseGroup.Contains(Knights[i]) || siegeGroup.Contains(Knights[i])))
			  {
				
				siegeGroup.Add(Knights[i]);
			}

		}
		for (int i=0; i<Priests.Count; i++) {
			if(Priests[i]==null)
			{
				Priests.RemoveAt(i);
				return;
			}
			else
			{
				if(alignment==1)
				{
					if(Priests[i].transform.position.x==priest1Spawnloc.x && Priests[i].transform.position.z==priest1Spawnloc.z)
					{
						Priests[i].GetComponent<unitCharacter>().moveTarget=new Vector3(team1MoveLoc.x,Priests[i].transform.position.y,team1MoveLoc.z);
					}
				}
				else
				{
					if(Priests[i].transform.position.x==priest2Spawnloc.x && Priests[i].transform.position.z==priest2Spawnloc.z)
					{
						Priests[i].GetComponent<unitCharacter>().moveTarget=new Vector3(team2MoveLoc.x,Priests[i].transform.position.y,team2MoveLoc.z);
					}
					
				}
			}
			Priests[i].GetComponent<unitCharacter>().alignment=alignment;
			unitData tempUnit=new unitData();
			tempUnit.thisUnitType=unitTypes.Priest;
			if(defenseGroup.Count<4 &&!(defenseGroup.Contains(Priests[i]) ||scoutGroup.Contains(Priests[i]) || attackGroup.Contains(Priests[i])|| siegeGroup.Contains(Priests[i])))
			{
				
				defenseGroup.Add(Priests[i]);
			}
			else if(attackGroup.Count<5 &&!(attackGroup.Contains(Priests[i])||scoutGroup.Contains(Priests[i]) || defenseGroup.Contains(Priests[i])|| siegeGroup.Contains(Priests[i])))
			{
			
				attackGroup.Add(Priests[i]);
			}
			else if(!(attackGroup.Contains(Priests[i])|| scoutGroup.Contains(Priests[i]) || defenseGroup.Contains(Priests[i]) || siegeGroup.Contains(Priests[i])))
			{
				
				siegeGroup.Add(Priests[i]);
			}
		}
		for (int i=0; i<Archers.Count; i++) {
			if(Archers[i]==null)
			{
				Archers.RemoveAt(i);
				return;
			}
			else
			{
				if(alignment==1)
				{
					if(Archers[i].transform.position.x==archer1Spawnloc.x && Archers[i].transform.position.z==archer1Spawnloc.z)
					{
						Archers[i].GetComponent<unitCharacter>().moveTarget=new Vector3(team1MoveLoc.x,Archers[i].transform.position.y,team1MoveLoc.z);
					}
				}
				else
				{
					if(Archers[i].transform.position.x==archer2Spawnloc.x && Archers[i].transform.position.z==archer2Spawnloc.z)
					{
						Archers[i].GetComponent<unitCharacter>().moveTarget=new Vector3(team2MoveLoc.x,Archers[i].transform.position.y,team2MoveLoc.z);
					}
					
				}
			}
			Archers[i].GetComponent<unitCharacter>().alignment=alignment;
			unitData tempUnit=new unitData();
			tempUnit.thisUnitType=unitTypes.Archer;
			if(scoutGroup.Count<2 && !(attackGroup.Contains(Archers[i])||scoutGroup.Contains(Archers[i]) || defenseGroup.Contains(Archers[i])|| siegeGroup.Contains(Archers[i]) ))
			{
				
				scoutGroup.Add(Archers[i]);
			}
			else if(attackGroup.Count<5 && !(attackGroup.Contains(Archers[i])||scoutGroup.Contains(Archers[i]) || defenseGroup.Contains(Archers[i])|| siegeGroup.Contains(Archers[i])))
			{
				
				attackGroup.Add(Archers[i]);
			}
			else if(!(attackGroup.Contains(Archers[i])|| scoutGroup.Contains(Archers[i]) || defenseGroup.Contains(Archers[i]) || siegeGroup.Contains(Archers[i])))
			{
				
				siegeGroup.Add(Archers[i]);
			}
		}
		int numUnits = Archers.Count + Knights.Count + Priests.Count;

				if (initailWaitTime <= 0) {
					if (trainUnitTime <= 0) {
						
							if (numUnits < unitMax) {

										trainUnitTime = 4.0f;
					if (old) {
						if(unitBeingBuilt>=buildUnitsOrder.Length|| buildUnitsOrder[unitBeingBuilt]==null)
						{
										int chosenUnit = Random.Range (1, 4);	

								if (chosenUnit == 1) {
												Invoke ("spawnKnight", 4.0f);
										} else if (chosenUnit == 2) {
												Invoke ("spawnArcher", 4.0f);	
										} else {
												Invoke ("spawnPriest", 4.0f);	
										}
		
								}
					
					else
					{
						if(buildUnitsOrder[unitBeingBuilt].theunit.thisUnitType==unitTypes.Archer)
						{
							Invoke ("spawnArcher", 4.0f);	
						}
						else if(buildUnitsOrder[unitBeingBuilt].theunit.thisUnitType==unitTypes.Knight)
						{
							Invoke ("spawnKnight", 4.0f);
						}
						else
						{
							Invoke ("spawnPriest", 4.0f);	
						}
						unitBeingBuilt++;
					}
				}
					else
					{
						int chosenUnit = Random.Range (1, 4);	
						
						if (chosenUnit == 1) {
							Invoke ("spawnKnight", 4.0f);
						} else if (chosenUnit == 2) {
							Invoke ("spawnArcher", 4.0f);	
						} else {
							Invoke ("spawnPriest", 4.0f);	
						}
						

					}
				}
			} else {
									trainUnitTime -= Time.deltaTime;
						}
		}else {
							initailWaitTime -= Time.deltaTime;		
					}

		Character res = FindObjectOfType<Character> ();
		if (res != null) {
						if (alignment == res.alignment) {

								Vector3 moveTarget = Vector3.zero;
				if (!res.isDead && res.health < (res.totalHealth * .6)) {
					moveTarget = res.transform.position;
								}
								for (int i = 0; i < attackGroup.Count; i++) {
					if (moveTarget != Vector3.zero && !attackGroup[i].GetComponent<unitCharacter> ().isRetreating &&!attackGroup[i].GetComponent<unitCharacter> ().isHealing) {
											attackGroup [i].GetComponent<unitCharacter> ().moveTarget = moveTarget;
									} else {
											attackGroup [i].GetComponent<unitCharacter> ().moveTarget = attackGroup [i].GetComponent<unitCharacter> ().moveTarget;
									}
								}
							if(!isTraining)
							{
									int start = 2;
									for (int i = 0; i < Priests.Count; i++) {
										Priests [i].GetComponent<FOG_Player> ().playerNum = start;
										Priests [i].GetComponent<FOG_Player> ().enabled = true;
										start++;
										
									}
									for (int i = 0; i < Archers.Count; i++) {
										Archers [i].GetComponent<FOG_Player> ().playerNum = start;
										Archers [i].GetComponent<FOG_Player> ().enabled = true;
										start++;
										
									}
									for (int i = 0; i < Knights.Count; i++) {
										Knights [i].GetComponent<FOG_Player> ().playerNum = start;
										Knights [i].GetComponent<FOG_Player> ().enabled = true;
										start++;
										
									}
							}
						}
				}
        defendBase(defenseGroup);
        protectTheBase();
		if (mb.health <= mb.totalHealth * .8) {
			defendBase(scoutGroup);
			
			defendBase(attackGroup);
		
		
		}

			if (siegeGroup.Count > 3) {
				
						siegeEnemies ();
		
				} 


		withDraw (scoutGroup);
		withDraw (attackGroup);
		withDraw (siegeGroup);

		battle (scoutGroup);
		battle (attackGroup);
		battle (siegeGroup);
		battle (defenseGroup);

		scoutMove ();
		evaluateBattles ();
	if (attackGroup.Count>= 5) {
			attackGroupReady=true;
				}
		if(attackGroupReady)
		{
		if (!ambushCompleted) {
						ambush ();
				} else if (ambushTime <= 0) {
						ambushCompleted = false;		
				} else {
			ambushTime-=Time.deltaTime;
				}
		}
	}
    int currentLoc = 0;
	void defendBase(List<GameObject> group)
	{
        if (patrolLocations.Count > 0)
        {
        for (int i = 0; i < group.Count; i++)
        {
            
            if (currentLoc >= patrolLocations.Count)
            {
                currentLoc = 0;


            }

            if (!group[i].GetComponent<unitCharacter>().engagedIncombat && group[i].GetComponent<unitCharacter>().attackTarget == null && group[i].GetComponent<unitCharacter>().moveTarget == group[i].transform.position)
                {


                    group[i].GetComponent<unitCharacter>().moveTarget = new Vector3(patrolLocations[currentLoc].x, group[i].transform.position.y, patrolLocations[currentLoc].z);

 currentLoc++;
                }

               



            }
        }
	}
    public void protectTheBase()
    {
        foreach (Vector3 item in mb.enemiesAttackingPos())
        {
            if (!patrolLocations.Contains(item) && mb.distance(patrolLocations, item))
            {
                patrolLocations.Add(item);
            }
        }
    }
	void patrollMove(GameObject un)
	{

		if (un.GetComponent<NavMeshAgent> ().remainingDistance <= 2&& !un.GetComponent<unitCharacter>().engagedIncombat) {
			Vector3 temp;
			if(alignment==1)
			{
				do {
					temp = new Vector3 (Random.Range (-124, -72), un.transform.position.y, Random.Range (-125, -75));
				} while((temp.x>-125&&temp.z>-126)|| (temp.x<-72&&temp.z<-74));
			}
			else
			{
				do {
					temp = new Vector3 (Random.Range (79, 127), un.transform.position.y, Random.Range (85, 126));
				} while((temp.x>78&&temp.z>84)|| (temp.x<127&&temp.z<127));
			}
			un.GetComponent<unitCharacter> ().moveTarget = temp;
		}
	}
	/*
	 * public previousGameInfo currentGame=new previousGameInfo();
	attackGroupUnit currentAttackGroup=new attackGroupUnit();
	scoutGroupUnit currentScoutGroup=new scoutGroupUnit();
	defenseGroupUnit currentDefenseGroup=new defenseGroupUnit();
	siegeGroupUnit currentSiegeGroup=new siegeGroupUnit();
	List<BattleData> battlesThisGame= new List<BattleData>();
	List<unitData> unitsBuilt = new List<unitData> ();
	List<battleInfo> battles=new List<battleInfo>();
	 */ 
	public void fillinUnitData()
	{
		for (int i = 0; i < Archers.Count; i++) {
			if(Archers[i]!=null)
			{
			Archers[i].GetComponent<unitCharacter>().updateData();
			updatUnitInfo(Archers[i].GetComponent<unitCharacter>().keyNum,Archers[i].GetComponent<unitCharacter>().myData);
			}
		}
		for (int i = 0; i < Knights.Count; i++) {
			if(Knights[i]!=null)
			{
			Knights[i].GetComponent<unitCharacter>().updateData();
			updatUnitInfo(Knights[i].GetComponent<unitCharacter>().keyNum,Knights[i].GetComponent<unitCharacter>().myData);
			}
		}
		for (int i = 0; i < Priests.Count; i++) {
			if(Priests[i]!=null)
			{
			Priests[i].GetComponent<unitCharacter>().updateData();
			updatUnitInfo(Priests[i].GetComponent<unitCharacter>().keyNum,Priests[i].GetComponent<unitCharacter>().myData);
			}
		}
	}
	public void save()
	{
		fillinUnitData ();
		BinaryFormatter bf = new BinaryFormatter ();
		File.Delete ("D:/Capstone game" + "/" + name + ".dat");
		FileStream file = File.Open ("D:/Capstone game" + "/" + name + ".dat", FileMode.OpenOrCreate);
		for (int i = 0; i < battles.Count; i++) {
			BattleData temp=new BattleData();
			temp.enemyUnits=battles[i].enemyUnits;
			temp.friendlyUnits=battles[i].friendlyUnits;
			temp.enemyUnitsSurvived=battles[i].enemyUnitsSurvived;
			temp.friendlyUnitsSurvived=battles[i].friendlyUnitsSurvived;
			temp.numEnemyUnits=battles[i].numEnemyUnits;
			temp.numFriendlyUnits=battles[i].numFriendlyUnits;
			temp.won=battles[i].won;
            
			battlesThisGame.Add(temp);
		

				}
        foreach (Vector3 vecs in patrolLocations)
        {
            if (!patrolInfo.Contains(new defensePatrolInfo(vecs.x, vecs.y, vecs.z)))
            {
                patrolInfo.Add(new defensePatrolInfo(vecs.x, vecs.y, vecs.z));
            }
        }
        previousGames.defenseData[alignment] = patrolInfo;
		currentGame.gameBattles.AddRange (battlesThisGame);
		currentGame.numFriendlyUnitsKilled = unitsBuilt.Count - (Archers.Count + Priests.Count + Knights.Count);
		currentGame.unitsBuilt = unitsBuilt;
        currentGame.ambusheData = theAmbushes;
		previousGames.previousGames.Add (currentGame);
		bf.Serialize (file, previousGames);
		file.Close();
		finishedSaving = true;

		}
	public void load()
	{
		if (File.Exists ("D:/Capstone game" + "/" + name + ".dat")) {
			BinaryFormatter bf=new BinaryFormatter();
			FileStream file=File.Open("D:/Capstone game" + "/" + name + ".dat",FileMode.Open);
			previousGames=(SaveInfo)bf.Deserialize(file);

			file.Close();

			old=true;
			int chosenSize=0;
			for (int i = 0; i < previousGames.previousGames.Count; i++) {

					for (int b = 0; b < previousGames.previousGames[i].unitsBuilt.Count; b++) {
						if(previousGames.previousGames[i].unitsBuilt.Count>chosenSize)
						{
							chosenSize=previousGames.previousGames[i].unitsBuilt.Count;
						}
					}
                    for (int c = 0; c < previousGames.previousGames[i].ambusheData.Count; c++)
                    {
                        if (!previousAmbushes.Contains(previousGames.previousGames[i].ambusheData[c]))
                        {
                            previousAmbushes.Add(previousGames.previousGames[i].ambusheData[c]);
                        }
                    }

			}
            patrolInfo = previousGames.defenseData[alignment];
            foreach (defensePatrolInfo dpi in patrolInfo)
            {
                patrolLocations.Add(dpi.giveLocation());

            }
			buildUnitsOrder=new unitsBuilt[chosenSize];
			findBestUnitCombo();
		}

	}

	private void findBestUnitCombo()
	{
		for (int i = 0; i < previousGames.previousGames.Count; i++) {

				for (int b = 0; b < previousGames.previousGames[i].unitsBuilt.Count; b++) {
					evaluateUnit(previousGames.previousGames[i].unitsBuilt[b],b);
					   

					
				}
			}

	}
	private void evaluateUnit(unitData theUnit,int pos)
	{

		int assistCount = 0;
		
		int killCount = 0;
		int deathCount = 0;
		for (int i = 0; i < previousGames.previousGames.Count; i++) {

			if(pos<previousGames.previousGames[i].unitsBuilt.Count)
			{
					if(previousGames.previousGames[i].unitsBuilt[pos]==theUnit)
					{
					killCount+=previousGames.previousGames[i].unitsBuilt[pos].numkills;
                    assistCount += previousGames.previousGames[i].unitsBuilt[pos].numAssists;      
					if(previousGames.previousGames[i].unitsBuilt[pos].died)
					{
						deathCount++;
					}
						
				   }
			}

		}

		float kilDethRatio = findRatio (killCount, deathCount);
        float assistDeathRatio = findRatio(assistCount, deathCount);
		unitsBuilt builtUnit=new unitsBuilt();
		builtUnit.theunit=theUnit;
		builtUnit.killDeatRatio = kilDethRatio;
        builtUnit.assistDeathRatio = assistDeathRatio;
        if (kilDethRatio > .5f && assistDeathRatio > .5f)
        {
						if (buildUnitsOrder [pos] == null) {
								buildUnitsOrder [pos] = builtUnit;
						} else {
								if (buildUnitsOrder [pos].killDeatRatio < builtUnit.killDeatRatio) {
										buildUnitsOrder [pos] = builtUnit;
								}
						}
        }
        else if (kilDethRatio > 1.0 && assistDeathRatio < .5f)
        {
				
				
						if (buildUnitsOrder [pos] == null) {
								buildUnitsOrder [pos] = builtUnit;
						} else {
								if (buildUnitsOrder [pos].killDeatRatio < builtUnit.killDeatRatio) {
										buildUnitsOrder [pos] = builtUnit;
								}
						}
				} else {
			builtUnit.theunit=differentUnit(builtUnit.theunit.thisUnitType);
			builtUnit.killDeatRatio=0;
			builtUnit.assistDeathRatio=0;
			buildUnitsOrder [pos]=builtUnit;

				}


	}
	public unitData differentUnit(unitTypes unWantedUnit)
	{
		unitData theReturnUnit = new unitData ();
		int chosenOne = Random.Range (0, 2);
		if (unitTypes.Archer == unWantedUnit) {
							if(chosenOne==0)
						{
							theReturnUnit.thisUnitType=unitTypes.Knight;
						}
						else{
								theReturnUnit.thisUnitType=unitTypes.Priest;
						}
		
				} else if (unitTypes.Knight == unWantedUnit) {
				
							if(chosenOne==0)
							{
								theReturnUnit.thisUnitType=unitTypes.Archer;
							}
							else{
								theReturnUnit.thisUnitType=unitTypes.Priest;
							}
				} else {
						if(chosenOne==0)
						{
							theReturnUnit.thisUnitType=unitTypes.Knight;
						}
						else{
							theReturnUnit.thisUnitType=unitTypes.Archer;
						}
				}
		return theReturnUnit;
		}
	public float findRatio(int kills, int deaths)
	{
		float ratio = 0;
		if (deaths == 0) {
						ratio = kills;		
		
				} else if (kills == 0 && deaths > 0) {
						ratio = -1.0f;		
				} else {
			ratio=(float)kills/(float)deaths;		
		}
		return ratio;
	}

	 
	private void moveUnits(Vector3 posistion,List<GameObject> theUnits)
	{
		for (int i = 0; i < theUnits.Count; i++) {
			theUnits[i].GetComponent<unitCharacter>().isRetreating=true;
			theUnits[i].GetComponent<unitCharacter>().moveTarget=new Vector3(posistion.x,theUnits[i].transform.position.y,posistion.z);
				}
		}
	private void moveUnits2(Vector3 posistion,List<BaseCharacter> theUnits)
	{
		for (int i = 0; i < theUnits.Count; i++) {
			if(theUnits[i]!=null)
			{
				if(theUnits[i].gameObject.GetComponent<unitCharacter>()!=null)
				{
					theUnits[i].isRetreating=true;

					theUnits[i].gameObject.GetComponent<unitCharacter>().moveTarget=new Vector3(posistion.x,theUnits[i].transform.position.y,posistion.z);
				}
			}
		}
	}
	private void siegeEnemies()
	{
		MainBase[] mb=FindObjectsOfType<MainBase>();
		for (int i=0; i<siegeGroup.Count; i++) {

			if(!siegeGroup[i].GetComponent<unitCharacter>().isHealing && !siegeGroup[i].GetComponent<unitCharacter>().isRetreating)
			{
				if(mb[0].alignment!=alignment)
				{

					siegeGroup[i].GetComponent<unitCharacter>().moveTarget=new Vector3(mb[0].gameObject.transform.position.x,siegeGroup[i].transform.position.y,mb[0].gameObject.transform.position.z);
				}
				else{
					siegeGroup[i].GetComponent<unitCharacter>().moveTarget=new Vector3(mb[1].gameObject.transform.position.x,siegeGroup[i].transform.position.y,mb[1].gameObject.transform.position.z);
				}
			}
		}
		}



	private void scoutMove()
	{
		for (int i = 0; i < scoutGroup.Count; i++) {
			if (!scoutGroup [i].GetComponent<unitCharacter> ().isRetreating || !scoutGroup [i].GetComponent<unitCharacter> ().isHealing) {
								if (scoutGroup [i].GetComponent<NavMeshAgent> ().remainingDistance <= 2) {
										Vector3 temp;
										do {
												temp = new Vector3 (Random.Range (-100, 100), scoutGroup [i].transform.position.y, Random.Range (-100, 100));
										} while((temp.x<=-52&&temp.z<-52)|| (temp.x>52&&temp.z>52));
										scoutGroup [i].GetComponent<unitCharacter> ().moveTarget = temp;
								}
						}
				}
		}
	private int determineEffectiveness(GameObject mainUnit, GameObject otherUnit)
	{
		int effectiveness = 0;
		unitCharacter mainUnit2 = mainUnit.GetComponent<unitCharacter> ();
		unitCharacter otherUnit2 = otherUnit.GetComponent<unitCharacter> ();

		if (mainUnit2.attackRange > otherUnit2.attackRange) {
			effectiveness++;		
		}
		if (mainUnit2.damage > otherUnit2.damage) {
			effectiveness++;		
		}
		if (mainUnit2.fireRate > otherUnit2.fireRate) {
			effectiveness++;
		}
		if(mainUnit2.totalHealth>otherUnit2.totalHealth)
		{
			effectiveness++;
		}
		return effectiveness;
	}
	private void abandonBattle(battleInfo b)
	{
		for (int i = 0; i < previousGames.previousGames.Count; i++) {
			for (int j = 0; j < previousGames.previousGames[i].gameBattles.Count; j++) {
				if(previousGames.previousGames[i].gameBattles[j].friendlyUnits.Count==b.friendlyUnits.Count && 
				   previousGames.previousGames[i].gameBattles[j].enemyUnits.Count==b.enemyUnits.Count){
					if(CompareWithUnitTypes(b.friendlyUnits,previousGames.previousGames[i].gameBattles[j].friendlyUnits)&& CompareWithUnitTypes(b.enemyUnits,previousGames.previousGames[i].gameBattles[j].enemyUnits)
					   && !previousGames.previousGames[i].gameBattles[j].won)
					{
						if(alignment==1)
						{
							moveUnits2(team1MoveLoc ,b.friendlyUnits2);
						}
						else
						{
							moveUnits2(team2MoveLoc ,b.friendlyUnits2);
						}
						return;
					}
				}
			}
				}
	}
	bool CompareWithUnitTypes(List<unitData> one,List<unitData> two)
	{
		List<unitTypes> onesTypes = new List<unitTypes> ();
		List<unitTypes> twosTypes = new List<unitTypes> ();
		for (int i = 0; i < one.Count; i++) {
			onesTypes.Add(one[i].thisUnitType);
		}
		for (int i = 0; i < two.Count; i++) {
			twosTypes.Add(two[i].thisUnitType);
		}
		return UnorderedEqual (onesTypes, twosTypes);
	}
	bool UnorderedEqual(ICollection<unitTypes> a, ICollection<unitTypes> b)
	{
		// 1
		// Require that the counts are equal
		if (a.Count != b.Count)
		{
			return false;
		}
		// 2
		// Initialize new Dictionary of the type
		Dictionary<unitTypes, int> d = new Dictionary<unitTypes, int>();
		// 3
		// Add each key's frequency from collection A to the Dictionary
		foreach (unitTypes item in a)
		{
			int c;
			if (d.TryGetValue(item, out c))
			{
				d[item] = c + 1;
			}
			else
			{
				d.Add(item, 1);
			}
		}
		// 4
		// Add each key's frequency from collection B to the Dictionary
		// Return early if we detect a mismatch
		foreach (unitTypes item in b)
		{
			int c;
			if (d.TryGetValue(item, out c))
			{
				if (c == 0)
				{
					return false;
				}
				else
				{
					d[item] = c - 1;
				}
			}
			else
			{
				// Not in dictionary
				return false;
			}
		}
		// 5
		// Verify that all frequencies are zero
		foreach (int v in d.Values)
		{
			if (v != 0)
			{
				return false;
			}
		}
		// 6
		// We know the collections are equal
		return true;
	}


	private void evaluateBattles()
	{
		for (int b = 0; b < battles.Count; b++) {
			if(!battles[b].evaluated)
			{
				abandonBattle( battles[b]);
				if(battles[b].friendlyUnits2.Count<=0  )
				{
					battles[b].won=false;
					battles[b].evaluated=true;
					battles[b].friendlyUnitsSurvived=0;
					battles[b].enemyUnitsSurvived=battles[b].enemyUnits.Count-battles[b].enemyUnits2.Count;
				}
				else if(battles[b].enemyUnits2.Count<=0)
				{
					battles[b].won=true;
					battles[b].evaluated=true;
					battles[b].enemyUnitsSurvived=0;
					battles[b].friendlyUnitsSurvived=battles[b].friendlyUnits.Count-battles[b].friendlyUnits2.Count;
				}
				else
				{
					for (int i = 0; i < battles[b].friendlyUnits2.Count; i++) {
						if(battles[b].friendlyUnits2[i].isRetreating)
						{
							battles[b].won=false;
							battles[b].evaluated=true;
							battles[b].friendlyUnitsSurvived=battles[b].friendlyUnits.Count-battles[b].friendlyUnits2.Count;
							battles[b].enemyUnitsSurvived=battles[b].enemyUnits.Count-battles[b].enemyUnits2.Count;
							return;
						}
					}
					for (int i = 0; i < battles[b].enemyUnits2.Count; i++) {
						if(battles[b].enemyUnits2[i].isRetreating)
						{
							battles[b].won=true;
							battles[b].evaluated=true;
							battles[b].friendlyUnitsSurvived=battles[b].friendlyUnits.Count-battles[b].friendlyUnits2.Count;
							battles[b].enemyUnitsSurvived=battles[b].enemyUnits.Count-battles[b].enemyUnits2.Count;
							return;
						}
					}
				}
			}
		}
	}
	private bool isValidBattle(BaseCharacter bc)
	{
		bool valid = true;
		for (int b = 0; b < battles.Count; b++) {
						if (!battles [b].evaluated && battles [b].friendlyUnits2.Contains (bc)) {
				valid=false;
						}
				}
		return valid;
		}
	private battleInfo battle(List<GameObject> groups)
	{
		BaseCharacter[] allUnits=FindObjectsOfType<BaseCharacter>();



			for (int i = 0; i < groups.Count; i++) {
			if(groups[i].GetComponent<unitCharacter>().engagedIncombat)
			{
			
				if(isValidBattle(groups[i].GetComponent<BaseCharacter>()))
				{
					battleInfo b=new battleInfo();
						unitData temp=groups[i].GetComponent<BaseCharacter>().myData;

					b.friendlyUnits2.Add(groups[i].GetComponent<BaseCharacter>());
					b.friendlyUnits.Add(temp);
						for (int j = 0; j < allUnits.Length; j++) {
							temp=allUnits[j].myData;
							if(allUnits[j].alignment==alignment && (allUnits[j].transform.position-groups[i].transform.position).magnitude<=20 && allUnits[j].damage!=0)
							{
							b.friendlyUnits2.Add(allUnits[j]);
							
							b.friendlyUnits.Add(temp);
							
							}
							else if(allUnits[j].alignment!=alignment && (allUnits[j].transform.position-groups[i].transform.position).magnitude<=20 && allUnits[j].damage!=0)
							{

							b.enemyUnits2.Add(allUnits[j]);
							b.enemyUnits.Add(temp);
							}

					}
					b.numEnemyUnits=b.enemyUnits.Count;
					b.numFriendlyUnits=b.friendlyUnits.Count;
					if(b.numFriendlyUnits>=3 && b.numEnemyUnits>=3)
					{
					battles.Add(b);
                    return b;
					}
				}
			}

		}
            return null;
	}
	private void withDraw(List<GameObject> groups)
	{
		BaseCharacter[] allUnits=FindObjectsOfType<BaseCharacter>();


		for (int i = 0; i < groups.Count; i++) {
			int canWin=0;
			int cantWin = 0;
			List<BaseCharacter> friendlies = new List<BaseCharacter> ();
			List<BaseCharacter> enemies = new List<BaseCharacter> ();
			if(groups[i].GetComponent<unitCharacter>().engagedIncombat)
			{
				friendlies.Add(groups[i].GetComponent<BaseCharacter>());
					for (int j = 0; j < allUnits.Length; j++) {
						if(allUnits[j].alignment==alignment && (allUnits[j].transform.position-groups[i].transform.position).magnitude<=20 && allUnits[j].damage!=0)
						{
							friendlies.Add(allUnits[j]);
							canWin++;
						}
						else if(allUnits[j].alignment!=alignment && (allUnits[j].transform.position-groups[i].transform.position).magnitude<=20 && allUnits[j].damage!=0)
						{
							enemies.Add(allUnits[j]);
							cantWin++;
						}
					}
				if(evaluateEffectiveNess(friendlies)>=evaluateEffectiveNess(enemies))
				{
					canWin++;
				}
				else
				{
					cantWin++;
				}
				if(canWin<cantWin)
				{
					for (int j = 0; j < friendlies.Count; j++) {
						if(friendlies[j].gameObject.GetComponent<unitCharacter>()!=null)
						{
							if(alignment==1)
							{
								friendlies[j].gameObject.GetComponent<unitCharacter>().moveTarget=new Vector3(team1MoveLoc.x,friendlies[j].transform.position.y,team1MoveLoc.z);

							}
							else{
								friendlies[j].gameObject.GetComponent<unitCharacter>().moveTarget=new Vector3(team2MoveLoc.x,friendlies[j].transform.position.y,team2MoveLoc.z);
							}
							friendlies[j].gameObject.GetComponent<unitCharacter>().attackTarget=null;
							friendlies[j].gameObject.GetComponent<unitCharacter>().isRetreating=true;
						}
					}
				}


			}
		}
	}
	private int evaluateEffectiveNess(List<BaseCharacter> bc)
	{
		int evalResult = 0;
		for (int i = 0; i < bc.Count; i++) {
			if(bc[i].health<bc[i].totalHealth*.5)
			{
				evalResult-=1;
			}
			else if(bc[i].health>bc[i].totalHealth*.75)
			{
				evalResult+=1;
			}
		}
		return evalResult;
	}
	Vector3 ambushPosition=new Vector3();
	bool ambushRetreat=false;
	private void ambush()
	{
        ambushInfo tempAmbush = new ambushInfo();
        battleInfo ambushBattle = new battleInfo();
		if (!ambushSetUpCompleted) {
			setUpAmbush();		
		}
        tempAmbush.ambushPositionX = ambushPosition.x;
        tempAmbush.ambushPositionY = ambushPosition.y;
        tempAmbush.ambushPositionZ = ambushPosition.z;
		if (attackGroup [0].GetComponent<unitCharacter> ().engagedIncombat) {
						attackGroup [0].GetComponent<unitCharacter> ().moveTarget = new Vector3 (ambushPosition.x, attackGroup [0].transform.position.y, ambushPosition.z);
			
						attackGroup [0].GetComponent<unitCharacter> ().attackTarget = null;
						attackGroup [0].GetComponent<unitCharacter> ().isRetreating = true;
			ambushHasBeenInitiated=true;
			ambushRetreat=true;
				} else {
			baitMovement();		
		}
		if (ambushRetreat) {
			attackGroup [0].GetComponent<unitCharacter> ().attackTarget = null;
			attackGroup [0].GetComponent<unitCharacter> ().isRetreating = true;	
			attackGroup [0].GetComponent<unitCharacter> ().moveTarget = new Vector3 (ambushPosition.x, attackGroup [0].transform.position.y, ambushPosition.z);
            tempAmbush.baitInitaitePosX = attackGroup[0].transform.position.x;
            tempAmbush.baitInitaitePosY = attackGroup[0].transform.position.y;
            tempAmbush.baitInitaitePosZ = attackGroup[0].transform.position.z;

				
		
		}
		if ((attackGroup [0].transform.position - new Vector3 (ambushPosition.x, attackGroup [0].transform.position.y, ambushPosition.z)).magnitude < 5.0f) {
          ambushBattle  =  battle(attackGroup);
				}
      
          
                tempAmbush.wasSuccesFull = true;
                ambushWasCompleted();
                if (!theAmbushes.Contains(tempAmbush))
                {
                    theAmbushes.Add(tempAmbush);
                }
            
        }

	private void ambushWasCompleted()
	{
		if (ambushPosition != Vector3.zero && ambushHasBeenInitiated) {
			ambushCompleted=true;
			ambushSetUpCompleted=false;
			ambushTime=45.0f;
				}
		}
    int ambushCount = 0;
	private void baitMovement()
	{
       
		if (attackGroup[0].GetComponent<NavMeshAgent> ().remainingDistance <= 2) {
			Vector3 temp;
			do {
				temp = new Vector3 (Random.Range (-100, 100), attackGroup[0].transform.position.y, Random.Range (-100, 100));
			} while((temp.x<=-15.854&&temp.z<-52)|| (temp.x>52&&temp.z>52));
			attackGroup[0].GetComponent<unitCharacter> ().moveTarget = temp;
		}
	}
    
    private void setUpAmbush()
    {

        if (!old || ambushCount>=previousAmbushes.Count)
        {
            do
            {
                ambushPosition = new Vector3(Random.Range(-100, 100), attackGroup[0].transform.position.y, Random.Range(-100, 100));
            } while ((ambushPosition.x <= -52 && ambushPosition.z < -52) || (ambushPosition.x > -14.314 && ambushPosition.z > 52));

            for (int i = 0; i < attackGroup.Count; i++)
            {

                if (i == 0)
                {
                    attackGroup[i].GetComponent<unitCharacter>().isBait = true;
                }
                attackGroup[i].GetComponent<unitCharacter>().moveTarget = new Vector3(ambushPosition.x, attackGroup[i].transform.position.y, ambushPosition.z);

                attackGroup[i].GetComponent<unitCharacter>().attackTarget = null;
                attackGroup[i].GetComponent<unitCharacter>().isRetreating = true;
            }
            ambushSetUpCompleted = true;
        }
        else
        {
            ambushPosition = new Vector3(previousAmbushes[ambushCount].ambushPositionX, previousAmbushes[ambushCount].ambushPositionY, previousAmbushes[ambushCount].ambushPositionZ);
            for (int i = 0; i < attackGroup.Count; i++)
            {

                if (i == 0)
                {
                    attackGroup[i].GetComponent<unitCharacter>().isBait = true;
                }
                attackGroup[i].GetComponent<unitCharacter>().moveTarget = new Vector3(ambushPosition.x, attackGroup[i].transform.position.y, ambushPosition.z);

                attackGroup[i].GetComponent<unitCharacter>().attackTarget = null;
                attackGroup[i].GetComponent<unitCharacter>().isRetreating = true;
            }

            attackGroup[0].GetComponent<unitCharacter>().moveTarget = new Vector3(previousAmbushes[ambushCount].baitInitaitePosX, previousAmbushes[ambushCount].baitInitaitePosY, previousAmbushes[ambushCount].baitInitaitePosZ);
            ambushCount++;

        }
    }

}
public class unitsBuilt
{
	public unitData theunit=new unitData();
	public float assistDeathRatio;
	public float killDeatRatio;
}
