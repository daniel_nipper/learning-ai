﻿using UnityEngine;
using System.Collections;

public class BaseCharacter : MonoBehaviour {
	public int damage;
	public int health;
	public int totalHealth;
	public int alignment;
	public float radius;
	public bool isRetreating;
	public bool isHealing;
	public int numKills=0;
    public int numAssists = 0;
	public unitData myData= new unitData();
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
