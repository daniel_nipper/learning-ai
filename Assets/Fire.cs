﻿using UnityEngine;
using System.Collections;

public class Fire : MonoBehaviour {

	// Use this for initialization
	public GameObject target;
	public BaseCharacter attacker;
	public int damage;
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (target != null) {
						transform.LookAt (target.transform.position);
						transform.position = Vector3.MoveTowards (transform.position, target.transform.position, 7.0f * Time.deltaTime);
						if ((transform.position - target.transform.position).magnitude < .5) {
								
										target.GetComponent<BaseCharacter> ().health -= damage;
                                        if (target.GetComponent<BaseCharacter>().health <= 0 && target.GetComponent<BaseCharacter>().health + damage > 0)
                                        {

                                            attacker.numKills++;
                                        }
                                        else if (target.GetComponent<BaseCharacter>().health + damage > 0)
                                        {
                                            attacker.numAssists++;
                                        }
								Destroy (gameObject);
						}
				} else {
				
			Destroy (gameObject);
		}
	}
}
