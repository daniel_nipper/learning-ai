﻿using UnityEngine;
using System.Collections;

public class Character : BaseCharacter {
	Animator characterAnimator;

	public GameObject field;
	//public int health;
	//public int damage;
	public float attackRange ;
	public int sightRange;
	public GameObject attackTarget;
	public GameObject arrow;
	public bool isArcher;
	public bool isDead;
	public float fireRate=2;
	float fireWait;
	Vector3 lastSight;
	public float deadTime=5.0f;
	public float timeLeftDead=0.0f;
	private Vector3 prePOS;
	NavMeshAgent nm;
	NavMeshObstacle no;
	GUITexture healthBar;
	Vector3 position;

	// Use this for initialization
	void Start () {
	
		healthBar = GameObject.Find ("playerHealth").guiTexture;
		healthBar.pixelInset = new Rect (0, 0, 0, 0);
		position = transform.position;
		characterAnimator = GetComponent<Animator> ();
		nm = GetComponent<NavMeshAgent> ();
		no = GetComponent<NavMeshObstacle> ();
		prePOS =transform.position;
		fireWait = fireRate;
		field = GameObject.Find ("field");
		isDead = false;
	}

	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.Space)) {
				
			Camera.main.transform.position=new Vector3(transform.position.x,Camera.main.transform.position.y,transform.position.z-20);
		
		}
		if (health > totalHealth) {
			health=totalHealth;
				}
		if (health <= 0) {
			isDead=true;		
		}
		if (!isDead) {
			timeLeftDead=0.0f;
						if (Input.GetMouseButtonDown (1)) {
			
								Ray intersectRay = Camera.main.ScreenPointToRay (Input.mousePosition);
								RaycastHit hitField = new RaycastHit ();
								RaycastHit hitField2 = new RaycastHit ();
								if (field.collider.Raycast (intersectRay, out hitField, 100.0f)) {
										nm.enabled = true;
										no.enabled = false;
								}
								bool clickedOnUnit = false;
								BaseCharacter[] temp = FindObjectsOfType<BaseCharacter> ();
								intersectRay = Camera.main.ScreenPointToRay (Input.mousePosition);
								for (int i=0; i<temp.Length; i++) {
										if (alignment != temp [i].alignment) {
												if (temp [i].collider.Raycast (intersectRay, out hitField2, 100.0f)) {
														clickedOnUnit = true;
														attackTarget = temp [i].gameObject;
												}
										}
								}
								if (!clickedOnUnit) {
										attackTarget = null;
								}

				
								position = new Vector3 (hitField.point.x, transform.position.y, hitField.point.z);
				
				
						}

						if (attackTarget != null) {
								if (prePOS == transform.position && attackTarget == null) {
										characterAnimator.Play ("Idle");
										nm.enabled = false;
										no.enabled = true;
								} else if (attackTarget != null) {
										if ((attackTarget.transform.position - transform.position).magnitude <= attackRange+ attackTarget.GetComponent<BaseCharacter>().radius) {
												if (nm.enabled) {
														nm.SetDestination (transform.position);
												}
												transform.LookAt (new Vector3 (attackTarget.transform.position.x, transform.position.y, attackTarget.transform.position.z));
												characterAnimator.Play ("Attack");

												prePOS = Vector3.zero;
												nm.enabled = false;
												no.enabled = true;
												if (fireWait >= fireRate) {

														if (isArcher) {
											
																fireArrow ();
														
														
														} else {
																attackTarget.GetComponent<unitCharacter> ().health -= damage;
																if(attackTarget.GetComponent<BaseCharacter>().health<=0)
																{
																	numKills++;
																}
														}
														fireWait = 0.0f;
												}
					
										} else {
												characterAnimator.Play ("Walk");
												nm.enabled = true;
												no.enabled = false;
					
										}
								} else {
										nm.enabled = true;
										no.enabled = false;
										characterAnimator.Play ("Walk");
								}
						
						
								if ((attackTarget.transform.position - transform.position).magnitude > attackRange) {
					
					
										if ((attackTarget.transform.position - transform.position).magnitude < sightRange) {
												prePOS = attackTarget.transform.position;
												lastSight = prePOS;
										} else {
												prePOS = lastSight;
												attackTarget = null;
										}
								}

				  
		
						} else {

								Vector3 posDistance = (transform.position - position);
								float val = posDistance.sqrMagnitude;
								if (val > 1.0f) {
										characterAnimator.Play ("Walk");

										nm.enabled = true;
										no.enabled = false;
								} else {
										nm.enabled = false;
										no.enabled = true;
										characterAnimator.Play ("Idle");
								}

						

								//rigidbody.velocity=Vector3.zero;
								//rigidbody.angularVelocity=Vector3.zero;
						}
						if (nm.enabled) {


								nm.SetDestination (position);
								transform.LookAt (nm.nextPosition);
								prePOS = nm.nextPosition;
						} else {
								prePOS = transform.position;
						}

				
						fireWait += Time.deltaTime;
						//transform.position=Vector3.MoveTowards(transform.position,position,((6.0f)*Time.deltaTime));
				} else {
			gameObject.active=false;

			Invoke("revive",10);

			}
		textHealth ();
	}
//	bool positionComparer(Vector3 playerPos,Vector3 targetPos)
//	{
//		bool isNotEqual = false;
//		if (System.Math.Round (playerPos.x, 2) != System.Math.Round (targetPos.x, 2)) {
//			isNotEqual = true;		
//		
//				} else if (System.Math.Round (playerPos.y, 2) != System.Math.Round (targetPos.y, 2)) {
//			isNotEqual = true;
//		
//		}
//		else if (System.Math.Round (playerPos.z, 2) != System.Math.Round (targetPos.z, 2)) {
//			isNotEqual = true;
//			
//		}
//		return isNotEqual;
//		}
	void fireArrow()
	{
		GameObject temp = Instantiate (arrow, transform.position, Quaternion.identity)as GameObject;
		temp.GetComponent<Fire> ().target = attackTarget;
		temp.GetComponent<Fire> ().damage = damage;
		temp.GetComponent<Fire> ().attacker = this;
	}
	void revive()
	{
	
		gameObject.active=true;
		isDead=false;
		health=totalHealth;
		if(alignment==1)
		{
			transform.position=new Vector3(-69.99163f,3.933498f,-89.88019f);
		}
		position = transform.position;
	}
	void textHealth()
	{
		int percent = (int)((float)health / (float)totalHealth* 100);
		percent = (100 - percent) * 4;
		healthBar.pixelInset = new Rect (0, 0, -percent, 0);
		}
}
