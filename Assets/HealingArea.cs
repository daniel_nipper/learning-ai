﻿using UnityEngine;
using System.Collections;


public class HealingArea : MonoBehaviour {
	public int alignment=1;
	public int healRate=2;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		BaseCharacter[] friendlies = FindObjectsOfType<BaseCharacter> ();
		for (int i = 0; i < friendlies.Length; i++) {

						if (friendlies [i].alignment == alignment && friendlies [i].health < friendlies [i].totalHealth) {
								if (friendlies [i].GetComponent<MainBase> () == null) {
										bool intersects = collider.bounds.Intersects (friendlies [i].collider.bounds);

										if (intersects) {
												friendlies [i].health += healRate;
										}
								}
						}
				}
	}
}
