﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class ChangeHealth : MonoBehaviour {

	public BaseCharacter unit;
	// Use this for initialization
	public enum SelectedAxis{
		xAxis,
		yAxis,
		zAxis
	}
	public SelectedAxis selectedAxis = SelectedAxis.xAxis;
	
	// Target
	public Image image;

	
	// Parameters
	public float minValue = 0.0f;
	public float maxValue = 1.0f;
	public Color minColor = Color.red;
	public Color maxColor = Color.green;
	
	// The default image is the one in the gameObject
	void Start(){
		if (image == null){
			image = GetComponent<Image>();
		}

	}
	
	void Update () {
		SetHealthVisual ((float)unit.health / (float)unit.totalHealth);
		switch (selectedAxis){
		case SelectedAxis.xAxis:
			// Lerp color depending on the scale factor
			image.color = Color.Lerp(minColor,
			                         maxColor,
			                         Mathf.Lerp(minValue,
			           maxValue,
			           transform.localScale.x));
			break;
		case SelectedAxis.yAxis:
			// Lerp color depending on the scale factor
			image.color = Color.Lerp(minColor,
			                         maxColor,
			                         Mathf.Lerp(minValue,
			           maxValue, transform.localScale.y));
			break;
		case SelectedAxis.zAxis:
			// Lerp color depending on the scale factor
			image.color = Color.Lerp(minColor,
			                         maxColor,
			                         Mathf.Lerp(minValue,
			           maxValue,
			           transform.localScale.z));
			break;
		}
	}
	public void SetHealthVisual(float healthNormalized){
		transform.localScale = new Vector3( healthNormalized,
		                                             transform.localScale.y,
		                                             transform.localScale.z);
	}

}
