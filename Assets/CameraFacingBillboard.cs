﻿using UnityEngine;
using System.Collections;

public class CameraFacingBillboard : MonoBehaviour {

	Camera referenceCamera;
	public GameObject character;
	public GameObject heroEnemy;
	public enum Axis {up, down, left, right, forward, back};
	public bool reverseFace = false; 
	public Axis axis = Axis.up; 
	
	// return a direction based upon chosen axis
	public Vector3 GetAxis (Axis refAxis)
	{
		switch (refAxis)
		{
		case Axis.down:
			return Vector3.down; 
		case Axis.forward:
			return Vector3.forward; 
		case Axis.back:
			return Vector3.back; 
		case Axis.left:
			return Vector3.left; 
		case Axis.right:
			return Vector3.right; 
		}
		
		// default is Vector3.up
		return Vector3.up; 		
	}
	
	void  Awake ()
	{
		// if no camera referenced, grab the main camera
		if (!referenceCamera)
			referenceCamera = Camera.main; 
	}
	
	void  Update ()
	{
		if (character != null) {
						if (character.GetComponent<unitCharacter> ().isArcher) {
								transform.position = new Vector3 (character.transform.position.x - 1.0f, character.transform.position.y + 2.0f, character.transform.position.z);
						} else if (character.GetComponent<unitCharacter> ().isPriest) {
								transform.position = new Vector3 (character.transform.position.x - 1.0f, character.transform.position.y + 2.0f, character.transform.position.z);
						} else {
								transform.position = new Vector3 (character.transform.position.x - 1.2f, character.transform.position.y + 3.0f, character.transform.position.z);
						}
				} else if (heroEnemy != null) {
			if (heroEnemy.GetComponent<unitHero> ().isArcher) {
				transform.position = new Vector3 (heroEnemy.transform.position.x - 1.0f, heroEnemy.transform.position.y + 2.0f, heroEnemy.transform.position.z);
			} else {
				transform.position = new Vector3 (heroEnemy.transform.position.x - 1.0f, heroEnemy.transform.position.y + 4.0f, heroEnemy.transform.position.z);
			}
		}
		// rotates the object relative to the camera
		Vector3 targetPos = transform.position + referenceCamera.transform.rotation * (reverseFace ? Vector3.forward : Vector3.back) ;
		Vector3 targetOrientation = referenceCamera.transform.rotation * GetAxis(axis);
		transform.LookAt (targetPos, targetOrientation);
	}
}

