﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class AI_Hero : MonoBehaviour {

	// Use this for initialization
	public GameObject[] inHeroes;
	public int alignment;
	public GameObject hero;
	public bool isTraining=false;
	Vector3 team1MoveLoc = new Vector3 (-70.77477f,8.490299f,-84.8895f);
	Vector3 team2MoveLoc = new Vector3 (87.23208f,8.490299f,65.54948f);
	void Start () {

	int chosen=	Random.Range (0, inHeroes.Length);
		if (alignment == 2) {
						if (chosen == 0) {
								hero = Instantiate (inHeroes [0], new Vector3 (61.61699f, 4.462234f, 98.2905f), Quaternion.identity)as GameObject;
						} else {
								hero = Instantiate (inHeroes [1], new Vector3 (61.61699f, 3.948404f, 98.2905f), Quaternion.identity)as GameObject;
				
						}
				} else {

						if (chosen == 0) {
								hero = Instantiate (inHeroes [0], new Vector3 (-61.61699f, 4.462234f, -98.2905f), Quaternion.identity)as GameObject;
						} else {
								hero = Instantiate (inHeroes [1], new Vector3 (-61.61699f, 3.948404f, -98.2905f), Quaternion.identity)as GameObject;
			
						}
				}
		hero.GetComponent<unitHero> ().alignment = alignment;

				


	}
	public void TrainIngStart()
	{
		int chosen=	Random.Range (0, inHeroes.Length);
		if (alignment == 2) {
			if (chosen == 0) {
				hero = Instantiate (inHeroes [0], new Vector3 (61.61699f, 4.462234f, 98.2905f), Quaternion.identity)as GameObject;
			} else {
				hero = Instantiate (inHeroes [1], new Vector3 (61.61699f, 3.948404f, 98.2905f), Quaternion.identity)as GameObject;
				
			}
		} else {
			
			if (chosen == 0) {
				hero = Instantiate (inHeroes [0], new Vector3 (-61.61699f, 4.462234f, -98.2905f), Quaternion.identity)as GameObject;
			} else {
				hero = Instantiate (inHeroes [1], new Vector3 (-61.61699f, 3.948404f, -98.2905f), Quaternion.identity)as GameObject;
				
			}
		}
		hero.GetComponent<unitHero> ().alignment = alignment;
		
		

		}
	// Update is called once per frame
	void Update () {
		heroMove ();
		withDraw ();
	}
	private void heroMove()
	{
		if (hero != null) {
						if (!hero.GetComponent<unitHero> ().isRetreating && !hero.GetComponent<unitHero> ().isHealing && !hero.GetComponent<unitHero> ().isDead) {
								if (hero.GetComponent<NavMeshAgent> ().remainingDistance <= 2) {
										Vector3 temp;
										do {
												temp = new Vector3 (Random.Range (-100, 100), hero.transform.position.y, Random.Range (-100, 100));
										} while((temp.x<=-52&&temp.z<-52)|| (temp.x>52&&temp.y>52));
										hero.GetComponent<unitHero> ().moveTarget = temp;
								}
						}
				}
	}
	private void withDraw()
	{
		if (hero != null) {
						BaseCharacter[] allUnits = FindObjectsOfType<BaseCharacter> ();
		

						int canWin = 0;
						int cantWin = 0;
						List<BaseCharacter> friendlies = new List<BaseCharacter> ();
						List<BaseCharacter> enemies = new List<BaseCharacter> ();
						if (hero.GetComponent<unitHero> ().engagedIncombat) {
								friendlies.Add (hero.GetComponent<BaseCharacter> ());
								for (int j = 0; j < allUnits.Length; j++) {
										if (allUnits [j].gameObject != hero) {
												if (allUnits [j].alignment == alignment && (allUnits [j].transform.position - hero.transform.position).magnitude <= 20 && allUnits [j].damage != 0) {
														friendlies.Add (allUnits [j]);
														canWin++;
												} else if (allUnits [j].alignment != alignment && (allUnits [j].transform.position - hero.transform.position).magnitude <= 20 && allUnits [j].damage != 0) {
														enemies.Add (allUnits [j]);
														cantWin++;
												}
										}
								}
								if (evaluateEffectiveNess (friendlies) >= evaluateEffectiveNess (enemies)) {
										canWin++;
								} else {
										cantWin++;
								}
								if (canWin < cantWin) {
										for (int j = 0; j < friendlies.Count; j++) {
												if (friendlies [j].gameObject.GetComponent<unitCharacter> () != null) {
														if (alignment == 1) {
																hero.GetComponent<unitHero> ().moveTarget = new Vector3 (team1MoveLoc.x, friendlies [j].transform.position.y, team1MoveLoc.z);
								
														} else {
																hero.GetComponent<unitHero> ().moveTarget = new Vector3 (team2MoveLoc.x, friendlies [j].transform.position.y, team2MoveLoc.z);
														}
														hero.GetComponent<unitHero> ().attackTarget = null;
														hero.GetComponent<unitHero> ().isRetreating = true;
												}
										}
								}
				
				
						}
				}
	}
	private int evaluateEffectiveNess(List<BaseCharacter> bc)
	{
		int evalResult = 0;
		for (int i = 0; i < bc.Count; i++) {
			if(bc[i].health<bc[i].totalHealth*.5)
			{
				evalResult-=1;
			}
			else if(bc[i].health>bc[i].totalHealth*.75)
			{
				evalResult+=1;
			}
		}
		return evalResult;
	}
	public void AIHeroReset()
	{
		Destroy (hero);
		TrainIngStart ();
	}

}
