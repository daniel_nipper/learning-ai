﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;

[Serializable]
public class SaveInfo {
	public List<previousGameInfo> previousGames { get; set;}
    public Dictionary<int, List<defensePatrolInfo>> defenseData { get; set; }
    public SaveInfo(List<previousGameInfo> pg, Dictionary<int, List<defensePatrolInfo>> dpi)
	{previousGames = pg;
    defenseData = dpi;
		}
	public SaveInfo()
	{
		previousGames = new List<previousGameInfo> ();
        defenseData = new Dictionary<int, List<defensePatrolInfo>>();
		}
   
}
[Serializable]
public class previousGameInfo{
	public List<unitData> unitsBuilt{ get; set; }
	
	
	public bool wonGame{ get; set;}
	public int numFriendlyUnitsKilled{ get; set;}
	public heroType alliedHero{ get; set;} 
	public List<BattleData> gameBattles{ get; set; }
    public List<ambushInfo> ambusheData { get; set; }
    

	public previousGameInfo(){
		unitsBuilt = new List<unitData> ();
	
		wonGame = false;
		numFriendlyUnitsKilled = 0;
		alliedHero = heroType.Archer;
		gameBattles = new List<BattleData> ();
		}
    public previousGameInfo(List<unitData> ub, bool w, int fren, heroType ah, List<BattleData> gb, List<ambushInfo> aD)
	{
		unitsBuilt = ub;
        ambusheData = aD;
		wonGame = w;
		numFriendlyUnitsKilled = fren;
		alliedHero = ah;
		gameBattles = gb;
       

		}
}
[Serializable]
public enum heroType {Archer,Berzerker};
[Serializable]
public enum unitTypes {Archer,Knight,Priest};
[Serializable]
public class unitData {
	public float wasCreatedat{ get; set;}
	public unitTypes thisUnitType{ get; set;}
	public int numkills { get; set; }
    public int numAssists { get; set; }
	public bool died { get; set; }
	public unitData(float wc, unitTypes ut, int killCount,bool death,int assistCount)
	{
		wasCreatedat = wc;
		thisUnitType = ut;
		numkills = killCount;
        numAssists = assistCount;
		died = death;
		}
	public unitData()
	{
		wasCreatedat = 0.0f;
		thisUnitType = unitTypes.Archer;
	}
	public override bool Equals (object obj)
	{
		unitData temp = obj as unitData;
		if (temp == null) {
			return false;		
		}

		if (temp.thisUnitType == this.thisUnitType) {
						return true;		
				} else {
			return false;		
		}
	}

}
[Serializable]
public class BattleData{
	public bool won{ get; set;}
	public int numFriendlyUnits{ get; set;}
	public int numEnemyUnits{ get; set;}
	public List<unitData> friendlyUnits{ get; set;}
	public List<unitData> enemyUnits{ get; set;}
	public int friendlyUnitsSurvived{ get; set;}
	public int enemyUnitsSurvived{ get; set;}
	public BattleData(bool w,int f, int n, List<unitData> frend,List<unitData> enem,int sf,int se )
	{
		won = w;
		numFriendlyUnits = f;
		numEnemyUnits = n;
		friendlyUnits = frend;
		enemyUnits = enem;
		friendlyUnitsSurvived = sf;
		enemyUnitsSurvived = se;

		}
	public BattleData( )
	{
		won = false;
		numFriendlyUnits=0;
		numEnemyUnits = 0;
		friendlyUnits = new List<unitData> ();
		enemyUnits = new List<unitData> ();
		friendlyUnitsSurvived = 0;
		enemyUnitsSurvived = 0;
	}
}
[Serializable]
public class ambushInfo {
    public bool wasSuccesFull = false;
    public float ambushPositionX = new float();
    public float ambushPositionY = new float();
    public float ambushPositionZ = new float();

    public float baitInitaitePosX = new float();
    public float baitInitaitePosY = new float();
    public float baitInitaitePosZ = new float();
    

}
public class battleInfo{
	public bool won=false;
	public bool evaluated=false;
	public int numFriendlyUnits=0;
	public int numEnemyUnits=0;
	public List<unitData> friendlyUnits = new List<unitData> ();
	public List<unitData> enemyUnits = new List<unitData> ();
	public List<BaseCharacter> friendlyUnits2 = new List<BaseCharacter> ();
	public List<BaseCharacter> enemyUnits2 = new List<BaseCharacter> ();
	public int friendlyUnitsSurvived=0;
	public int enemyUnitsSurvived=0;
}
[Serializable]
public class defensePatrolInfo
{
    public float x { get; set; }
    public float y { get; set; }
    public float z { get; set; }
    public defensePatrolInfo(float x, float y, float z)
    {
        this.x = x;
        this.y = y;
        this.z = z;
    }
    public void setUsingVector3(Vector3 invec)
    {
        this.x = invec.x;
        this.y = invec.y;
        this.z = invec.z;
    }
    public Vector3 giveLocation()
    {
        return new Vector3(this.x, this.y, this.z);
    }
}
