﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
public class Demo4AI : MonoBehaviour {

	public GameObject[] units;
	bool[] hasSpawned={false,false,false};
	Animator priest;
	public int alignment;
	float time=3.0f;
	int unitSpawning=0;
	bool ambushCompleted=false;
	bool ambushSetUpCompleted=false;
	bool canMakeAmbush=false;
	bool ambushHasBeenInitiated=false;
	bool attackGroupReady=false;
	float ambushTime=0.0f;
	public string name;
	public Unit_Spawner spawner;
	private List<GameObject> Archers=new List<GameObject>();
	private List<GameObject> Knights=new List<GameObject>();
	private List<GameObject> Priests=new List<GameObject>();
	public List<GameObject> scoutGroup = new List<GameObject> ();
	public List<GameObject> attackGroup = new List<GameObject> ();
	public List<GameObject> defenseGroup = new List<GameObject> ();
	public List<GameObject> siegeGroup = new List<GameObject> ();
	public int[] knightEffectiveness = new int[2];
	public int[] archerEffectiveness = new int[2];
	public int[] priestEffectiveness = new int[2];
	unitsBuilt[] buildUnitsOrder;
	public MainBase mb;
	int numKnights=0;
	int numArchers=0;
	int numPriests=0;
	bool old=false;
	Vector3 team1MoveLoc = new Vector3 (-70.77477f,8.490299f,-84.8895f);
	Vector3 team2MoveLoc = new Vector3 (87.23208f,8.490299f,65.54948f);

	Vector3 archer1Spawnloc = new Vector3 (-85.36117f, 1.76716f, -90.28188f);
	Vector3 archer2Spawnloc = new Vector3 (85.36117f, 1.76716f, 90.28188f);

	Vector3 knight1Spawnloc = new Vector3 (-95.3123f, 1.76716f, -81.62152f);
	Vector3 knight2Spawnloc = new Vector3 (95.3123f, 1.76716f, 81.62152f);

	Vector3 priest1Spawnloc = new Vector3 (-81.10491f, 1.76716f, -97.35266f);
	Vector3 priest2Spawnloc = new Vector3 (81.10491f, 1.76716f, 97.35266f);

	float trainUnitTime=0.0f;
	public int unitMax=15;
	float initailWaitTime=20.0f;
/// <save data>
	SaveInfo previousGames=new SaveInfo();
	public previousGameInfo currentGame=new previousGameInfo();
	List<BattleData> battlesThisGame= new List<BattleData>();
	List<unitData> unitsBuilt = new List<unitData> ();
	List<battleInfo> battles=new List<battleInfo>();
    List<ambushInfo> theAmbushes = new List<ambushInfo>();
    List<ambushInfo> previousAmbushes = new List<ambushInfo>();
    List<Vector3> patrolLocations = new List<Vector3>();
    List<defensePatrolInfo> patrolInfo = new List<defensePatrolInfo>();
	int unitBeingBuilt=0;
	public bool isTraining=false;
	int currentUnitCount=0;
	public bool finishedSaving=false;
/// T/////////// </summary>





	void Start () {

						load ();
								
	}
	public void updatUnitInfo(int code, unitData theData)
	{
		for (int i = 0; i < unitsBuilt.Count; i++) {
			if(unitsBuilt[i].wasCreatedat==theData.wasCreatedat && unitsBuilt[i].thisUnitType==theData.thisUnitType)
			{
				unitsBuilt [i].died = theData.died;
				unitsBuilt[i].numkills=theData.numkills;
			}
				}

		}
	
	void spawnArcher()
	{
		if (alignment == 1) {
			Archers.Add (spawner.Spawnunit (units [2],archer1Spawnloc));

				} else {
			Archers.Add (spawner.Spawnunit (units [2], archer2Spawnloc));

		}
		unitData temp=new unitData();
		temp.thisUnitType = unitTypes.Archer;
		temp.wasCreatedat = Time.timeSinceLevelLoad;
		unitsBuilt.Add (temp);

		Archers [Archers.Count - 1].GetComponent<unitCharacter> ().myData = temp;
		Archers [Archers.Count - 1].layer = 10;
		Archers [Archers.Count - 1].GetComponent<unitCharacter> ().keyNum = (unitsBuilt.Count-1);
		currentUnitCount++;
		hasSpawned [0] = true;

	}
	void spawnPriest()
	{
		if (alignment == 1) {
			Priests.Add (spawner.Spawnunit (units [0], priest1Spawnloc));

				} else {
			Priests.Add (spawner.Spawnunit (units [0],priest2Spawnloc ));

		}
		unitData temp=new unitData();
		temp.thisUnitType = unitTypes.Priest;
		temp.wasCreatedat = Time.timeSinceLevelLoad;
		unitsBuilt.Add (temp);
	
		Priests [Priests.Count - 1].GetComponent<unitCharacter> ().myData = temp;
		Priests [Priests.Count - 1].GetComponent<unitCharacter> ().keyNum = (unitsBuilt.Count-1);
		currentUnitCount++;
		hasSpawned [1] = true;
		}
	void spawnKnight()
	{
		hasSpawned [2] = true;
		if (alignment == 1) {
				
			Knights.Add (spawner.Spawnunit (units [1],knight1Spawnloc ));

				} else {
			Knights.Add (spawner.Spawnunit (units [1],knight2Spawnloc ));


		}
		unitData temp=new unitData();
		temp.thisUnitType = unitTypes.Knight;
		temp.wasCreatedat = Time.timeSinceLevelLoad;
		unitsBuilt.Add (temp);

		Knights [Knights.Count - 1].GetComponent<unitCharacter> ().myData = temp;
		Knights [Knights.Count - 1].GetComponent<unitCharacter> ().keyNum = (unitsBuilt.Count-1);
		currentUnitCount++;
		}
	
	// Update is called once per frame
	void Update () {

		//different groups
		for (int i = 0; i < defenseGroup.Count; i++) {
					if(defenseGroup[i]==null)
					{
						defenseGroup.RemoveAt(i);
					}

				}
	
		for (int i=0; i<Knights.Count; i++) {
			if(Knights[i]==null)
			{
				Knights.RemoveAt(i);
				return;
			}
			else
			{
				if(alignment==1)
				{
					if(Knights[i].transform.position.x==knight1Spawnloc.x && Knights[i].transform.position.z==knight1Spawnloc.z)
					{
						Knights[i].GetComponent<unitCharacter>().moveTarget=new Vector3(team1MoveLoc.x,Knights[i].transform.position.y,team1MoveLoc.z);
					}
				}
				else
				{
					if(Knights[i].transform.position.x==knight2Spawnloc.x && Knights[i].transform.position.z==knight2Spawnloc.z)
					{
						Knights[i].GetComponent<unitCharacter>().moveTarget=new Vector3(team2MoveLoc.x,Knights[i].transform.position.y,team2MoveLoc.z);
					}

					}
			}
			Knights[i].GetComponent<unitCharacter>().alignment=alignment;
			unitData tempUnit=new unitData();
			tempUnit.thisUnitType=unitTypes.Knight;
			
			if(defenseGroup.Count<4 &&!(defenseGroup.Contains(Knights[i]) ||scoutGroup.Contains(Knights[i]) || attackGroup.Contains(Knights[i])|| siegeGroup.Contains(Knights[i])))
			{

				
				defenseGroup.Add(Knights[i]);
			}
			

		}
		for (int i=0; i<Priests.Count; i++) {
			if(Priests[i]==null)
			{
				Priests.RemoveAt(i);
				return;
			}
			else
			{
				if(alignment==1)
				{
					if(Priests[i].transform.position.x==priest1Spawnloc.x && Priests[i].transform.position.z==priest1Spawnloc.z)
					{
						Priests[i].GetComponent<unitCharacter>().moveTarget=new Vector3(team1MoveLoc.x,Priests[i].transform.position.y,team1MoveLoc.z);
					}
				}
				else
				{
					if(Priests[i].transform.position.x==priest2Spawnloc.x && Priests[i].transform.position.z==priest2Spawnloc.z)
					{
						Priests[i].GetComponent<unitCharacter>().moveTarget=new Vector3(team2MoveLoc.x,Priests[i].transform.position.y,team2MoveLoc.z);
					}
					
				}
			}
			Priests[i].GetComponent<unitCharacter>().alignment=alignment;
			unitData tempUnit=new unitData();
			tempUnit.thisUnitType=unitTypes.Priest;
			if(defenseGroup.Count<8 &&!(defenseGroup.Contains(Priests[i]) ||scoutGroup.Contains(Priests[i]) || attackGroup.Contains(Priests[i])|| siegeGroup.Contains(Priests[i])))
			{
				
				defenseGroup.Add(Priests[i]);
			}
		
		}
		for (int i=0; i<Archers.Count; i++) {
			if(Archers[i]==null)
			{
				Archers.RemoveAt(i);
				return;
			}
			else
			{
				if(alignment==1)
				{
					if(Archers[i].transform.position.x==archer1Spawnloc.x && Archers[i].transform.position.z==archer1Spawnloc.z)
					{
						Archers[i].GetComponent<unitCharacter>().moveTarget=new Vector3(team1MoveLoc.x,Archers[i].transform.position.y,team1MoveLoc.z);
					}
				}
				else
				{
					if(Archers[i].transform.position.x==archer2Spawnloc.x && Archers[i].transform.position.z==archer2Spawnloc.z)
					{
						Archers[i].GetComponent<unitCharacter>().moveTarget=new Vector3(team2MoveLoc.x,Archers[i].transform.position.y,team2MoveLoc.z);
					}
					
				}
			}
			Archers[i].GetComponent<unitCharacter>().alignment=alignment;
			unitData tempUnit=new unitData();
			tempUnit.thisUnitType=unitTypes.Archer;
            if (defenseGroup.Count < 8 && !(defenseGroup.Contains(Archers[i]) || scoutGroup.Contains(Archers[i]) || attackGroup.Contains(Archers[i]) || siegeGroup.Contains(Archers[i])))
            {

                defenseGroup.Add(Priests[i]);
            }
		}
        defendBase(defenseGroup);
        protectTheBase();
        //if (mb.health <= mb.totalHealth * .8) {
			
        //    defendBase(defenseGroup);
        //    defendBase(attackGroup);
		
		
        //}

		


	
	}
    int currentLoc = 0;
	void defendBase(List<GameObject> group)
	{


        if (patrolLocations.Count > 0)
        {
        for (int i = 0; i < group.Count; i++)
        {
            
            if (currentLoc >= patrolLocations.Count)
            {
                currentLoc = 0;


            }

            if (!group[i].GetComponent<unitCharacter>().engagedIncombat && group[i].GetComponent<unitCharacter>().attackTarget == null && group[i].GetComponent<unitCharacter>().moveTarget == group[i].transform.position)
                {


                    group[i].GetComponent<unitCharacter>().moveTarget = new Vector3(patrolLocations[currentLoc].x, group[i].transform.position.y, patrolLocations[currentLoc].z);
					currentLoc++;

                }

                



            }
        }
	}
    public void protectTheBase()
    {
        foreach (Vector3 item in mb.enemiesAttackingPos())
        {
            if (!patrolLocations.Contains(item) && mb.distance(patrolLocations, item))
            {
                patrolLocations.Add(item);
            }
        }
    }
    public void save()
    {
     
        BinaryFormatter bf = new BinaryFormatter();
        File.Delete("D:/Capstone game" + "/" + name + ".dat");
        FileStream file = File.Open("D:/Capstone game" + "/" + name + ".dat", FileMode.OpenOrCreate);
        for (int i = 0; i < battles.Count; i++)
        {
            BattleData temp = new BattleData();
            temp.enemyUnits = battles[i].enemyUnits;
            temp.friendlyUnits = battles[i].friendlyUnits;
            temp.enemyUnitsSurvived = battles[i].enemyUnitsSurvived;
            temp.friendlyUnitsSurvived = battles[i].friendlyUnitsSurvived;
            temp.numEnemyUnits = battles[i].numEnemyUnits;
            temp.numFriendlyUnits = battles[i].numFriendlyUnits;
            temp.won = battles[i].won;

            battlesThisGame.Add(temp);


        }

        currentGame.gameBattles.AddRange(battlesThisGame);
        currentGame.numFriendlyUnitsKilled = unitsBuilt.Count - (Archers.Count + Priests.Count + Knights.Count);
        currentGame.unitsBuilt = unitsBuilt;
        currentGame.ambusheData = theAmbushes;
        foreach (Vector3 vecs in patrolLocations)
        {
            if(!patrolInfo.Contains(new defensePatrolInfo(vecs.x,vecs.y,vecs.z)))
            {
                patrolInfo.Add(new defensePatrolInfo(vecs.x, vecs.y, vecs.z));
            }
        }
        previousGames.defenseData[alignment] = patrolInfo;
        previousGames.previousGames.Add(currentGame);
        bf.Serialize(file, previousGames);
        file.Close();
        finishedSaving = true;

    }
    public void load()
    {
        if (File.Exists("D:/Capstone game" + "/" + name + ".dat"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open("D:/Capstone game" + "/" + name + ".dat", FileMode.Open);
            previousGames = (SaveInfo)bf.Deserialize(file);

            file.Close();

            old = true;
            int chosenSize = 0;
            for (int i = 0; i < previousGames.previousGames.Count; i++)
            {

                for (int b = 0; b < previousGames.previousGames[i].unitsBuilt.Count; b++)
                {
                    if (previousGames.previousGames[i].unitsBuilt.Count > chosenSize)
                    {
                        chosenSize = previousGames.previousGames[i].unitsBuilt.Count;
                    }
                }
                for (int c = 0; c < previousGames.previousGames[i].ambusheData.Count; c++)
                {
                    if (!previousAmbushes.Contains(previousGames.previousGames[i].ambusheData[c]))
                    {
                        previousAmbushes.Add(previousGames.previousGames[i].ambusheData[c]);
                    }
                }

            }
            patrolInfo = previousGames.defenseData[alignment];
            foreach ( defensePatrolInfo dpi in patrolInfo)
            {
                patrolLocations.Add(dpi.giveLocation());
                
            }


        }

    }
}
