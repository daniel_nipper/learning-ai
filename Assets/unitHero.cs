﻿using UnityEngine;
using System.Collections;

public class unitHero : BaseCharacter {
	public float attackRange;
	public int sightRange;
	public GameObject attackTarget;
	public Vector3 moveTarget;
	public GameObject arrow;
	public bool isArcher;
	public bool isPriest;
	public float fireRate;
	public bool engagedIncombat;
	public bool isDead;
	float fireWait;
	Vector3 lastSight;
	Animator an;
	BaseCharacter[] targets;
	NavMeshAgent nm;
	Light specialLight;
	// Use this for initialization
	void Start () {
		engagedIncombat = false;
		an = GetComponent<Animator> ();
		nm = GetComponent<NavMeshAgent> ();
		targets = FindObjectsOfType<BaseCharacter> ();
		fireWait = fireRate;
		specialLight = gameObject.GetComponent<Light> ();
		moveTarget = transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		if (!isDead) {
						if ((transform.position - moveTarget).magnitude < 5) {
								isRetreating = false;
						}
						if (moveTarget == transform.position && attackTarget == null) {
								an.Play ("Idle");
								isRetreating = false;
								engagedIncombat = false;
						} else if (attackTarget != null) {
								if ((new Vector3 (attackTarget.transform.position.x, transform.position.y, attackTarget.transform.position.z) - transform.position).magnitude <= attackRange) {
										nm.SetDestination (transform.position);
										nm.Stop ();
										engagedIncombat = true;
										transform.LookAt (new Vector3 (attackTarget.transform.position.x, transform.position.y, attackTarget.transform.position.z));
										an.Play ("Attack");
										moveTarget = Vector3.zero;
						
										if (fireWait >= fireRate) {
												if (isArcher || isPriest) {
														fireArrow ();
												} else {
														attackTarget.GetComponent<BaseCharacter> ().health -= damage;
													if(attackTarget.GetComponent<BaseCharacter>().health<=0)
													{
														numKills++;
													}
												}
												fireWait = 0.0f;
							
										}
						
								} else {
										an.Play ("Walk");
						
						
								}
						} else {
								engagedIncombat = false;
								an.Play ("Walk");
						}
						targets = FindObjectsOfType<BaseCharacter> ();
						if (!isRetreating && !isHealing) {
								if (attackTarget == null) {
										for (int i=0; i<targets.Length; i++) {
							
							
												if ((targets [i].transform.position - transform.position).magnitude < sightRange && targets [i].alignment != alignment && targets [i].gameObject.active) {
														attackTarget = targets [i].transform.gameObject;
														moveTarget = attackTarget.transform.position;
														lastSight = moveTarget;
												}
										}
						
								}
						}
						if (attackTarget == null && moveTarget == Vector3.zero) {
								moveTarget = transform.position;		
						}
						if (attackTarget != null) {
								if (!attackTarget.gameObject.active) {
										attackTarget = null;
										return;
								}
								if ((attackTarget.transform.position - transform.position).magnitude > attackRange) {
						
						
										if ((attackTarget.transform.position - transform.position).magnitude < sightRange) {
												moveTarget = new Vector3 (attackTarget.transform.position.x, transform.position.y, attackTarget.transform.position.z);
												lastSight = moveTarget;
										} else {
												moveTarget = lastSight;
												attackTarget = null;
										}
								}
						}
				
						if (moveTarget != Vector3.zero) {
								nm.SetDestination (moveTarget);
								transform.LookAt (nm.nextPosition);
						}
				
						beingMousedOver ();
						fireWait += Time.deltaTime;
				
						if (health <= 0) {
				isDead=true;
								//DestroyObject(gameObject);
						}
						if (health <= totalHealth * .5 && !engagedIncombat) {
								isHealing = true;
								HealingArea[] ha = FindObjectsOfType<HealingArea> ();
								if (ha [0].alignment == alignment) {
										moveTarget = new Vector3 (ha [0].transform.position.x, transform.position.y, ha [0].transform.position.z);
								} else {
										moveTarget = new Vector3 (ha [1].transform.position.x, transform.position.y, ha [1].transform.position.z);
								}
						}
						if (health >= totalHealth) {
								health = totalHealth;
								isHealing = false;
					
						}
				}
		else {
			gameObject.active=false;
			
			Invoke("revive",10);
			
		}
	}
	void fireArrow()
	{
		GameObject temp = Instantiate (arrow, transform.position, Quaternion.identity)as GameObject;
		temp.GetComponent<Fire> ().target = attackTarget;
		temp.GetComponent<Fire> ().damage = damage;
		temp.GetComponent<Fire> ().attacker = this;
	}
	void beingMousedOver()
	{

			
		if (alignment == 1) {
				
						light.color = Color.blue;
		
				} else {
				
			light.color = Color.red;
		}

			specialLight.enabled = true;
			

	}
	void revive()
	{
		
		gameObject.active=true;
		isDead=false;
		health=totalHealth;
		attackTarget = null;
		if(alignment==2)
		{
			transform.position=new Vector3(69.99163f,3.933498f,89.88019f);
		}
		moveTarget = transform.position;
	}
}
