﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameManagerScript : MonoBehaviour {
	float scaleTime=1.0f;
	public MainBase team1;
	public MainBase team2;
	public AI_units AI1;
	public AI_units AI2;
	public bool hasSaved=false;
	public bool isPaused=false;
	public Text gameSpeed;
	public bool isTraining=false;
	public AI_Hero hero1;
	public AI_Hero hero2;
	public Simulation sim;
	// Use this for initialization
	void Start () {
	
	}
	public void GameManagerReset()
	{

		AI1.AIReset ();
		AI2.AIReset ();
		team1.MainBaseReset ();
		team2.MainBaseReset ();
		hero1.AIHeroReset ();
		hero2.AIHeroReset ();
		hasSaved = false;
		}
	// Update is called once per frame
	void Update () {
		if (!hasSaved) {
			if(team1.health <500)
			{
				print("pause");
			}

						if (team1.health <= 0) {
			
								AI1.currentGame.wonGame = false;
								AI2.currentGame.wonGame = true;
								AI1.save ();
								AI2.save ();
								print("Team2");

								hasSaved = true;
							
								Application.Quit ();
						} else if (team2.health <= 0) {
								AI2.currentGame.wonGame = false;
								AI1.currentGame.wonGame = true;
								AI1.save ();
								AI2.save ();
								print("Team1");
								hasSaved = true;
								
								Application.Quit ();
						} 
			else if(Input.GetKeyDown(KeyCode.S))
			{
				AI2.currentGame.wonGame = true;
				AI1.currentGame.wonGame = true;
				AI1.save ();
				AI2.save ();
			
				hasSaved = true;

			}
				} else if(isTraining){
					if(AI1.finishedSaving && AI2.finishedSaving)
					{
						sim.simCompleted();
					}
				}
		if (!isTraining) {
			if (hasSaved) {
				Application.LoadLevel(2);
			}
						if (isPaused) {
								Time.timeScale = 0;
						} else {



								Time.timeScale = scaleTime;
						}

						gameSpeed.text = "" + Time.timeScale;
				}
	}
	public void pause()
	{
		isPaused = true;
		}
	public void play()
	{
		isPaused = false;
	}
	public void increaseSpeed()
	{
		if (scaleTime <= 5) {
			scaleTime+=.5f;		
		}
	}
	public void decreaseSpeed()
	{
		if (scaleTime-.5f > 0) {
			scaleTime-=.5f;		
		}
	}
}
