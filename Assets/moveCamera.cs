﻿using UnityEngine;
using System.Collections;

public class moveCamera : MonoBehaviour {

	public int boundry=50;
	public float speed=5.0f;

	float distance=20.0f;
	float sensitivity=50.0f;
	float damping=5.0f;
	float minFOV=20;
	float maxFOV=80;
	GameObject field;
	// Use this for initialization
	void Start () {
		distance = Camera.main.fieldOfView;
		field = GameObject.Find ("field");
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 temp = transform.position;
		Ray intersectRay = Camera.main.ScreenPointToRay (temp);
		RaycastHit hitField = new RaycastHit ();
		bool hit = false;
		if (field.collider.Raycast (intersectRay, out hitField, 100.0f)) {
			hit=true;
				}
		float moveSpeedHelper = 10.0f;
		if (hit) {
						if (Input.mousePosition.x >= Screen.width - boundry) {

								temp.x += speed * Time.deltaTime * moveSpeedHelper;		
						}
						if (Input.mousePosition.y >= Screen.height - boundry) {
								temp.z += speed * Time.deltaTime * moveSpeedHelper;		
						}
						if (Input.mousePosition.x <= boundry) {
								temp.x -= speed * Time.deltaTime * moveSpeedHelper;		
						}
						if (Input.mousePosition.y <= boundry) {
								temp.z -= speed * Time.deltaTime * moveSpeedHelper;		
						}
				} else {
		
			if (Input.mousePosition.x >= Screen.width - boundry && temp.x<field.transform.position.x) {
				
				temp.x += speed * Time.deltaTime * moveSpeedHelper;		
			}
			if (Input.mousePosition.y >= Screen.height - boundry && temp.z<field.transform.position.z) {
				temp.z += speed * Time.deltaTime * moveSpeedHelper;		
			}
			if (Input.mousePosition.x <= boundry && temp.x>field.transform.position.x) {
				temp.x -= speed * Time.deltaTime * moveSpeedHelper;		
			}
			if (Input.mousePosition.y <= boundry && temp.z>field.transform.position.z) {
				temp.z -= speed * Time.deltaTime * moveSpeedHelper;		
			}
		}

		//distance -= Input.GetAxis ("Mouse ScrollWheel") * sensitivity;
		//distance = Mathf.Clamp (distance, minFOV, maxFOV);
		//Camera.main.fieldOfView = Mathf.Lerp (Camera.main.fieldOfView, distance, Time.deltaTime * damping);
		transform.position = temp;
				
	}
}
