﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Indestructable : MonoBehaviour {
	public GameObject choice;
	// Use this for initialization
	public int chosen;
	public GameObject archer;
	public GameObject berzerker;
	public GameObject archerHero;
	public GameObject berzerkerHero;
	public Light archerLight;
	public Light berzerkerLight;
	public InputField ally;
	public InputField enemy;
	public Button start;
	string AI1;
	string AI2;
	 int count=0;
	void Start () {
		DontDestroyOnLoad (choice);
		start.interactable = false;
	}
	
	// Update is called once per frame
	void Update () {
	if (Application.loadedLevel == 1) {
			if(count==0)
			{
				if(GameObject.Find("A.I. Manager")!=null)
				{
					GameObject.Find("A.I. Manager").GetComponent<AI_units>().name=AI1;
				}
				if(GameObject.Find("A.I. Manager2")!=null)
				{
					GameObject.Find("A.I. Manager2").GetComponent<AI_units>().name=AI2;
				}

				if(chosen==0)
				{
					Instantiate(archerHero,new Vector3(-59.61699f,4.262234f,-98.2905f), Quaternion.identity);
				}
						else{
					Instantiate(berzerkerHero,new Vector3(-59.61699f,3.948404f,-98.2905f), Quaternion.identity);

						}
				count++;
			}
				} else {
			if (Input.GetMouseButtonDown (0)) {
				
				Ray intersectRay = Camera.main.ScreenPointToRay (Input.mousePosition);
				RaycastHit hitField = new RaycastHit ();
				if (archer.collider.Raycast (intersectRay, out hitField, 100.0f)) {
					print ("hit field");
					archerLight.light.enabled=true;
					berzerkerLight.light.enabled=false;
					start.interactable = true;
					chosen=0;

				}
				if (berzerker.collider.Raycast (intersectRay, out hitField, 100.0f)) {
					print ("hit field");
					archerLight.light.enabled=false;
					berzerkerLight.light.enabled=true;
					start.interactable = true;
					chosen=1;

				}
			}
		}
	}
	public string getAI1()
	{
		return AI1;
		}
	public string getAI2()
	{
		return AI2;
	}
	public void loadNextLevel()
	{
		AI1 = ally.text;
		AI2 = enemy.text;
		Application.LoadLevel(1);
		}

}
