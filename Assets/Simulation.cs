﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Simulation : MonoBehaviour {

	// Use this for initialization
	public InputField numSims;
	int numSimulations=0;
	private int numSimsCompleted=0;
	public GameManagerScript gm;
	public Button simRunButton;
	public Slider progress;
	public int timeNeeded=0;
	void Start () {
		Time.timeScale = 0;
		progress.value = 0;
	}
	
	// Update is called once per frame
	void Update () {
		if (numSimulations == numSimsCompleted) {

						simRunButton.interactable = true;	

				} else {
						simRunButton.interactable = false;	

				}
		Time.timeScale = timeNeeded;
	}
	public void simCompleted()
	{

		numSimsCompleted++;
		progress.value++;
		if (numSimulations == numSimsCompleted) {
			numSimsCompleted = 0;
			numSimulations = 0;
			progress.value=0;
			timeNeeded=0;
		}
		gm.GameManagerReset ();

	
		}
	public void setNumSimulations()
	{
		int.TryParse (numSims.text,out numSimulations);
		progress.maxValue = numSimulations;
		timeNeeded = 80;
		}
	public void loadSelectScreen()
	{
		Application.LoadLevel (0);
		}

}
