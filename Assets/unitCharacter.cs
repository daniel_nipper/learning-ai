﻿using UnityEngine;
using System.Collections;

public class unitCharacter : BaseCharacter {

	// Use this for initialization



	public float attackRange;
	public int sightRange;
	public GameObject attackTarget;
	public Vector3 moveTarget;
	public GameObject arrow;
	public bool isArcher;
	public bool isPriest;
	public float fireRate;
	public bool engagedIncombat;
	public bool isBait=false;
	public bool isAtbase=false;
    
	public int keyNum;
	AI_units master;
	MainBase[] mb;
	float fireWait;
	Vector3 lastSight;
	Demo2AI demo2Master;

	Animator an;
	BaseCharacter[] targets;
	NavMeshAgent nm;
	Light specialLight;

	void Start () {
		engagedIncombat = false;
		an = GetComponent<Animator> ();
		nm = GetComponent<NavMeshAgent> ();
		targets = FindObjectsOfType<BaseCharacter> ();
		fireWait = fireRate;
		specialLight = gameObject.GetComponent<Light> ();
		moveTarget = transform.position;
		mb=FindObjectsOfType<MainBase>();
		AI_units[] temp = GameObject.FindObjectsOfType<AI_units> ();
		Demo2AI[] temp2 = GameObject.FindObjectsOfType<Demo2AI> ();
		if (temp.Length>0) {
						if (temp [0].alignment == alignment) {
				
								master = temp [0];
						} else {
								master = temp [1];	
		
						}
				}
		if (temp2.Length>0) {
			if (temp2 [0].alignment == alignment) {
				
				demo2Master = temp2 [0];
			} else {
				demo2Master = temp2 [1];	
				
			}
		}

	}
	
	// Update is called once per frame
	void Update () {
		if ((transform.position - moveTarget).magnitude < 5) {
			if(isRetreating)
			{
				isRetreating=false;
               
			}
            moveTarget = transform.position;
		}
		if (moveTarget == transform.position && attackTarget == null) {
						an.Play ("Idle");
			isRetreating=false;
			engagedIncombat=false;
				} else if (attackTarget != null) {
                    float attackDistance = 0;
                    if (attackTarget.GetComponent<MainBase>() != null )
                    {
                        attackDistance = 5f;
                    }
			if((new Vector3(attackTarget.transform.position.x,transform.position.y,attackTarget.transform.position.z)-transform.position).magnitude-attackDistance<=attackRange)
			{
				nm.SetDestination(transform.position);
				nm.Stop();
				engagedIncombat=true;
				transform.LookAt(new Vector3(attackTarget.transform.position.x,transform.position.y,attackTarget.transform.position.z));
				an.Play("Attack");
				moveTarget=Vector3.zero;

				if(fireWait>=fireRate)
				{
				if(isArcher || isPriest)
				{
						fireArrow();
					}
					else{
						attackTarget.GetComponent<BaseCharacter>().health-=damage;
                        if (attackTarget.GetComponent<BaseCharacter>().health <= 0)
                        {
                            numKills++;
                        }
                        else
                        {
                            numAssists++;
                        }
					}
					fireWait=0.0f;

				}
		
			}
			else
			{
				an.Play("Walk");


			}
		}
		else
		{
			engagedIncombat=false;
			an.Play("Walk");
		}
		targets = FindObjectsOfType<BaseCharacter> ();
		if (!isRetreating && !isHealing) {
						if (attackTarget == null) {
								for (int i=0; i<targets.Length; i++) {
					
			
										if ((targets [i].transform.position - transform.position).magnitude < sightRange && targets [i].alignment != alignment && targets [i].gameObject.active) {
												attackTarget = targets [i].transform.gameObject;
												moveTarget = attackTarget.transform.position;
												lastSight = moveTarget;
										}
								}
			
						}
				}
		if (attackTarget == null && moveTarget==Vector3.zero) {
			moveTarget=transform.position;		
		}
		if (attackTarget != null) {
			if(!attackTarget.gameObject.active)
			{
				attackTarget=null;
				return;
			}
			if((attackTarget.transform.position - transform.position).magnitude > attackRange )
			{

			
				if ((attackTarget.transform.position - transform.position).magnitude < sightRange ) {
					moveTarget = new Vector3(attackTarget.transform.position.x,transform.position.y,attackTarget.transform.position.z);
								lastSight = moveTarget;
						} else {
								moveTarget = lastSight;
								attackTarget = null;
						}
			}
				}
	
		if (moveTarget != Vector3.zero) {
			nm.SetDestination(moveTarget);
			transform.LookAt(nm.nextPosition);
		}

		beingMousedOver ();
		fireWait += Time.deltaTime;

		if (health <= 0) {
			if(GameObject.Find("FogOfWarPlane")!=null)
			{
			GameObject.Find("FogOfWarPlane").GetComponent<Renderer>().material.SetVector("_Player"+GetComponent<FOG_Player>().playerNum+"_pos",new Vector4(-200.0f,0.0f,0.0f,1.0f));
			}
			myData.numkills=numKills;
			myData.died=true;
			if(master!=null)
			{
				master.updatUnitInfo(keyNum,myData);
			}
			else if(demo2Master!=null)
			{
				demo2Master.updatUnitInfo(keyNum,myData);
			}
				DestroyObject(gameObject);

		}
		if (health <= totalHealth * .5 && !engagedIncombat) {
			isHealing=true;
			HealingArea[] ha = FindObjectsOfType<HealingArea>();
			if(ha[0].alignment==alignment)
			{
				moveTarget=new Vector3(ha[0].transform.position.x,transform.position.y,ha[0].transform.position.z);
			}
			else{
				moveTarget=new Vector3(ha[1].transform.position.x,transform.position.y,ha[1].transform.position.z);
			}
		}
		if (health >= totalHealth) {
			health=totalHealth;
			isHealing=false;
		
		}
		//distanceFromBase ();
	}
	public void updateData()
	{
		myData.died = false;
		myData.numkills = numKills;
        myData.numAssists = numAssists;
		}
	void distanceFromBase()
	{

		if (alignment==1) {
			if( transform.position.x<=-78.589f && transform.position.z<=-71.935f)
			{
				isAtbase=true;
			}
			else
			{
				isAtbase=false;
			}
		}
		else {
			if( transform.position.x>=84.116f && transform.position.z>=78.395f)
			{
				isAtbase=true;
			}
			else
			{
				isAtbase=false;
			}
		}
			
		}
	void fireArrow()
	{
		GameObject temp = Instantiate (arrow, transform.position, Quaternion.identity)as GameObject;
		temp.GetComponent<Fire> ().target = attackTarget;
		temp.GetComponent<Fire> ().damage = damage;
		temp.GetComponent<Fire> ().attacker = this;
		}
	void beingMousedOver()
	{
		if (alignment == 1) {
			
			light.color = Color.blue;
			
		} else {
			
			light.color = Color.red;
		}

						specialLight.enabled = true;
		
		}
}
